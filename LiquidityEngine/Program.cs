﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
//using System.Data.OracleClient;
using System.Linq;
using System.Management.Instrumentation;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Globalization;
using System.Data.SqlClient;
using Extensions.DateTime;
using log4net;
using Oracle.ManagedDataAccess;
using Oracle.ManagedDataAccess.Client;

namespace TestConsole1
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("General");
        static DataTable LiquidityTable;
        static DataTable LiquidityTermsTable;
        static void Main(string[] args)
        {

            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                
                LiquidityTable = GetTable();
                LiquidityTermsTable = Table_Eval_Terms();
                var ds = new DataSet();
                Dictionary<int, List<LiqStruct>> LiqArrayWithPenalty = new Dictionary<int, List<LiqStruct>>();
                Dictionary<int, List<LiqStruct>> LiqArrayWithoutPenalty = new Dictionary<int, List<LiqStruct>>();

                DateTime[] LiqDates = new DateTime[4];
                if (DateTime.Now.Day >= 10)
                {
                    LiqDates[0] = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    LiqDates[1] = LiqDates[0].AddDays(-1);
                    LiqDates[2] = LiqDates[0].AddMonths(-1);
                    LiqDates[3] = LiqDates[2].AddDays(-1);
                }
                else
                {
                    LiqDates[0] = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddDays(-1);
                    LiqDates[1] = EndOfMonth(LiqDates[0].AddMonths(-1)).AddDays(1);
                    LiqDates[2] = EndOfMonth(LiqDates[0].AddMonths(-1));
                    LiqDates[3] = EndOfMonth(LiqDates[2].AddMonths(-1)).AddDays(1);
                }
                DateTime evaldate;// = Convert.ToDateTime("7/31/2017");
                string Query;
                using (var conn = new OracleConnection(ConfigurationManager.ConnectionStrings["Dev"].ConnectionString))
                {
                    conn.Open();
                    Query = " Delete from MIS_PERMAL.EVAL_LIQUIDITY_ANALYSIS where  Eval_Date >= '" + LiqDates[3].ToString("dd-MMM-yyyy") + "'";
                    var com = new OracleCommand(Query);
                    com.Connection = conn;
                    com.CommandType = CommandType.Text;

                    com.ExecuteNonQuery();
                    Query = " Delete from MIS_PERMAL.EVAL_LIQUIDITY_TERMS where  Eval_Date >= '" + LiqDates[3].ToString("dd-MMM-yyyy") + "'";
                    com = new OracleCommand(Query);
                    com.Connection = conn;
                    com.CommandType = CommandType.Text;

                    com.ExecuteNonQuery();
                }



                for (int j = 0; j < 4; j++)
                {
                    evaldate = LiqDates[j];
                    ds = new DataSet();
                    Query = "Select insight_product_id from MIS_PERMAL.Eval_Holdings  where eval_Date in ( Select max(Eval_Date) from MIS_PERMAL.Eval_Holdings where Eval_Date <= '" + evaldate.ToString("dd-MMM-yyyy") + "') group by insight_product_id  having Sum(EMV)>0";
                    GetData(ds, Query);

                    for (var i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Query = "Select Eval_ID from MIS_Permal.Eval_Portfolio where EVal_Date = '" + evaldate.ToString("dd-MMM-yyyy") + "' and insight_product_id=" + Convert.ToInt32(ds.Tables[0].Rows[i][0]);
                        using (var conn = new OracleConnection(ConfigurationManager.ConnectionStrings["Mgr"].ConnectionString))
                        {
                            conn.Open();
                            var com1 = new OracleCommand(Query);
                            com1.Connection = conn;
                            com1.CommandType = CommandType.Text;
                            var dss = new DataSet();
                            var da = new OracleDataAdapter();
                            da.SelectCommand = com1;

                            da.Fill(dss);
                            if (dss.Tables[0].Rows.Count > 0)
                            {
                                liquidity_Calculate(Convert.ToInt32(ds.Tables[0].Rows[i][0]), LiqArrayWithPenalty, LiqArrayWithoutPenalty, evaldate, Convert.ToInt32(dss.Tables[0].Rows[0][0]));
                                //liquidity_Calculate(1406, LiqArrayWithPenalty, LiqArrayWithoutPenalty, evaldate, Convert.ToInt32(dss.Tables[0].Rows[0][0]));
                            }
                            InsertDatatabeToDB();
                            LiquidityTable.Rows.Clear();
                            LiquidityTermsTable.Rows.Clear();
                        }


                    }
                    //Inserting Extreme Liquidity
                    InsertIntoDatabaseExtremeLiquidity(evaldate);
                }

            }

            catch (Exception e)
            {
                LogManager.GetLogger("General").Error("Error in Main module:" +e );
                log.Info("Error in Main module:" + e);
            }
         
        }

       

        static DataTable GetTable()
        {
          
            DataTable Table = new DataTable();
            Table.Columns.Add("APPROVAL_STATUS", typeof(string));
            Table.Columns.Add("WF_PORTFOLIO_ID", typeof(int));
            Table.Columns.Add("INSIGHT_PRODUCT_NAME", typeof(string));
            Table.Columns.Add("INSIGHT_PRODUCT_ID", typeof(int));
            Table.Columns.Add("EVAL_ID", typeof(int));
            Table.Columns.Add("EVAL_DATE", typeof(DateTime));
            Table.Columns.Add("WF_SECURITY_ID", typeof(int));
            Table.Columns.Add("WF_SECURITY_NAME", typeof(string));
            Table.Columns.Add("NEXT_REDEMPTION_DATE", typeof(DateTime));
            Table.Columns.Add("BASE_AMOUNT", typeof(double));
            Table.Columns.Add("WEIGHT", typeof(double));
            Table.Columns.Add("CUMULATIVE_WEIGHT", typeof(double));
            Table.Columns.Add("NAME", typeof(string));
            Table.Columns.Add("WF_TYPE_ID", typeof(int));
            Table.Columns.Add("CURRENCY_", typeof(string));
            Table.Columns.Add("IS_ILLIQUID", typeof(int));
            Table.Columns.Add("POSITION_TOTAL_BASE_VALUE", typeof(double));
            Table.Columns.Add("IS_SECURITY_PENDING", typeof(int));
            Table.Columns.Add("EVALUATION_RANK", typeof(int));
            Table.Columns.Add("IS_PINED", typeof(int));
            Table.Columns.Add("IS_OFFICIAL", typeof(int));
            Table.Columns.Add("POSITION", typeof(string));
            Table.Columns.Add("IS_OVERRIDEN", typeof(int));
            Table.Columns.Add("LIQUIDITY_MODEL", typeof(int));
            Table.Columns.Add("IS_RISK_DATA", typeof(int));
            Table.Columns.Add("MGR_FUND_ID", typeof(int));
            Table.Columns.Add("MGR_CLASS_ID", typeof(int));

            return Table;
        }


        static DataTable Table_Eval_Terms()
        {

            DataTable Table = new DataTable();
            Table.Columns.Add("APPROVAL_STATUS", typeof(string));
            Table.Columns.Add("WF_PORTFOLIO_ID", typeof(int));
            Table.Columns.Add("INSIGHT_PRODUCT_NAME", typeof(string));
            Table.Columns.Add("INSIGHT_PRODUCT_ID", typeof(int));
            Table.Columns.Add("EVAL_ID", typeof(int));
            Table.Columns.Add("EVAL_DATE", typeof(DateTime));
            Table.Columns.Add("WF_SECURITY_ID", typeof(int));
            Table.Columns.Add("WF_SECURITY_NAME", typeof(string));
            Table.Columns.Add("BASE_AMOUNT", typeof(double));
            Table.Columns.Add("WEIGHT", typeof(double));
            Table.Columns.Add("EVALUATION_RANK", typeof(int));
            Table.Columns.Add("IS_PINED", typeof(int));
            Table.Columns.Add("IS_OFFICIAL", typeof(int));
            Table.Columns.Add("FIRST_CASH_FLOW_1", typeof(DateTime));
            Table.Columns.Add("REDEMPTION_FREQUENCY", typeof(string));
            Table.Columns.Add("REDEMPTION_NOTICE", typeof(int));
            Table.Columns.Add("RED_PENALTY_WEIGHT_1", typeof(double));
            Table.Columns.Add("RED_PENALTY_WEIGHT_2", typeof(double));
            Table.Columns.Add("RED_PENALTY_WEIGHT_3", typeof(double));
            Table.Columns.Add("RED_GATE_WEIGHT_1", typeof(double));
            Table.Columns.Add("LOCK_UP_ENDS_DATE", typeof(DateTime));
            Table.Columns.Add("RED_NOTICE_DUE_DATE", typeof(DateTime));
            return Table;
        }

        public static void InsertDatatabeToDB()
        {
            DataTable With = new DataTable();
            DataTable Without = new DataTable();
            string Query;
            // With Penalty
            var result = from myrow in LiquidityTable.AsEnumerable() where myrow.Field<int>("LIQUIDITY_MODEL")==1 select myrow;
            With = result.AsDataView().ToTable();
            var Rows = (from row in With.AsEnumerable()
                        orderby row["Next_Redemption_Date"] ascending
                        select row);
           DataTable dataTable = Rows.AsDataView().ToTable();
           double CumWeight=0;
           for (int i = 0; i < dataTable.Rows.Count; i++)
           {
               if (Convert.ToInt16(dataTable.Rows[i][15]) == 1)
               {
                   dataTable.Rows[i][11] = 0;
               }
               else
               {
                   CumWeight = CumWeight + Convert.ToDouble(dataTable.Rows[i][10]);
                   dataTable.Rows[i][11] = CumWeight;
                }
           }
           if (dataTable.Rows.Count > 0)
           {
               InsertEvalLliquidityAanalysis(dataTable);
               //InsertEvalLliquidityTerms(LiquidityTermsTable);
           }

           var result1 = from myrow in LiquidityTable.AsEnumerable() where myrow.Field<int>("LIQUIDITY_MODEL") == 2 select myrow;
           Without = result1.AsDataView().ToTable();
           var Rows1 = (from row in Without.AsEnumerable()
                       orderby row["Next_Redemption_Date"] ascending
                       select row);
           dataTable = new DataTable();
           dataTable = Rows1.AsDataView().ToTable();
           CumWeight = 0;
           for (int i = 0; i < dataTable.Rows.Count; i++)
           {
               if (Convert.ToInt16(dataTable.Rows[i][15]) == 1)
               {
                   dataTable.Rows[i][11] = 0;
               }
               else
               {
                   CumWeight = CumWeight + Convert.ToDouble(dataTable.Rows[i][10]);
                   dataTable.Rows[i][11] = CumWeight;
               }
           }
           using (var conn = new OracleConnection(ConfigurationManager.ConnectionStrings["Dev"].ConnectionString))
           {
               conn.Open();
               for (int j = 0; j < dataTable.Rows.Count; j++)
               {
                   Query = "Insert into MIS_PERMAL.EVAL_LIQUIDITY_ANALYSIS Values('" + dataTable.Rows[j][0].ToString() + "'," + dataTable.Rows[j][1] + ",'" + dataTable.Rows[j][2].ToString().Replace("'", "''") + "'," + dataTable.Rows[j][3] + "," + dataTable.Rows[j][4] + ",'" + Convert.ToDateTime(dataTable.Rows[j][5]).ToString("dd-MMM-yyyy") + "'," + dataTable.Rows[j][6] + ",'" + dataTable.Rows[j][7].ToString() + "','" + Convert.ToDateTime(dataTable.Rows[j][8]).ToString("dd-MMM-yyyy") + "'," + dataTable.Rows[j][9] + "," + dataTable.Rows[j][10] + ","+dataTable.Rows[j][11] +",'" + dataTable.Rows[j][12].ToString() + "'," + dataTable.Rows[j][13] + ",'USD'," + dataTable.Rows[j][15] + "," + dataTable.Rows[j][16] + "," + dataTable.Rows[j][17] + ",1," + dataTable.Rows[j][19] + "," + dataTable.Rows[j][20] + ",0," + dataTable.Rows[j][22] + "," + dataTable.Rows[j][23] + ",1,0," + dataTable.Rows[j][26] + ")";
                   var com = new OracleCommand(Query);
                   com.Connection = conn;
                   com.CommandType = CommandType.Text;

                   com.ExecuteNonQuery();
               }
               conn.Close();
           }
        }
        public struct LiqStruct
        {
            public DateTime Redemption;
            public double BaseAmount;
            public double CumulativeWeight;
            public double weight;
            public string ManagerName;
            public double PositionBase;
            public int IsLiquid;
            public int WF_PortfolioID;
            public int WF_SecurityID;
            public string WF_SECURITY_NAME;
            public string Redemption_Freq;
            public double Gate;
            public string penalty;
            public string penalty1;
            public string penalty2;
            public string penalty3;
            public double penalty1weight;
            public double penalty2weight;
            public double penalty3weight;
            public DateTime RedNoticeDue;
            public DateTime FirstCashFlow;
            public DateTime LockupEndDate;
            public string Notice;
     
        };
        public static void InsertIntoDatabaseExtremeLiquidity(DateTime evaldate)
        {
            string query;
            DataSet ds;
            query = "insert into MIS_PERMAL.EVAL_LIQUIDITY_ANALYSIS select MIS_WFEXTRACT.V_EVAL_LIQUID_ANALYSIS_EX .* ,3 as LIQUIDITY_MODEL,1 as Is_Risk_Data,0 as MGR_FUND_ID, 0 as MGR_CLASS_ID from MIS_WFEXTRACT.V_EVAL_LIQUID_ANALYSIS_EX where eval_Date ='" + evaldate.ToString("dd-MMM-yyyy") + "'";
            using (var conn = new OracleConnection(ConfigurationManager.ConnectionStrings["Dev"].ConnectionString))
            {
                var com = new OracleCommand(query);
                conn.Open();
                com.Connection = conn;
                com.CommandType = CommandType.Text;

                com.ExecuteNonQuery();
            }

        }
        private static void InsertIntoDatabase(List<LiqStruct> ClassInfo, int ProductID, string Productname, string PenaltyType, DateTime evaldate, int SecurityPending, int EvalID, int MGR_FUND_CLASS_ID, int WF_TYPE_ID)
        {
         
            int isOverRidden = 0;
            string Query;
            int IsOfficial = 0;
                // Getting the Is_Offical value
            DataSet ds = new DataSet();
            Query = " Select Is_Official From MIS_PERMAL.Eval_Holdings where Insight_Product_ID = " + ProductID + " and Eval_Date = '" + evaldate.ToString("dd-MMM-yyyy") + "'";
            using (var conn = new OracleConnection(ConfigurationManager.ConnectionStrings["Dev"].ConnectionString))
            {
                ds = new DataSet();
                conn.Open();
                GetData(ds, Query);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    IsOfficial = Convert.ToInt32(ds.Tables[0].Rows[0][0]);

                }

               
            }
            int IS_Pined = 0;
            int LiquidityModel = 1;
            
            double PositionBaseValue;
            int IS_ILLIQUID = 0;
           
            //int WF_TYPE_ID = 1;
            string NAME,Security_Name;
            
            int WF_SECURITY_ID;
            string EVAL_DATE;
            int EVAL_ID;
            int INSIGHT_PRODUCT_ID = ProductID;
            string INSIGHT_PRODUCT_NAME = Productname;
            int WF_PORTFOLIO_ID = 0;
            
            string APPROVAL_STATUS = "Approved";
            ds = new DataSet();
           
            try
            {
               

                if (PenaltyType == "Without")
                        LiquidityModel = 1;
                    else if (PenaltyType == "With")
                        LiquidityModel = 2;
                    else
                        LiquidityModel = 3;
                for (int i = 0; i < ClassInfo.Count; i++)
                {
                    IS_ILLIQUID = ClassInfo[i].IsLiquid;

                    NAME = ClassInfo[i].ManagerName;
                    NAME = NAME.Replace("'", "''");
                    WF_SECURITY_ID = ClassInfo[i].WF_SecurityID;
                    WF_PORTFOLIO_ID = ClassInfo[i].WF_PortfolioID;
                    Security_Name = ClassInfo[i].WF_SECURITY_NAME;
                    Security_Name = Security_Name.Replace("'", "''");
                    if (SecurityPending == 1)
                        PositionBaseValue = 0;
                    else
                        PositionBaseValue = ClassInfo[i].PositionBase;
                    LiquidityTable.Rows.Add(new Object[] {"t:" + APPROVAL_STATUS , WF_PORTFOLIO_ID , INSIGHT_PRODUCT_NAME.Replace("'", "''") , INSIGHT_PRODUCT_ID, EvalID, evaldate.ToString("dd-MMM-yyyy") , + ClassInfo[i].WF_SecurityID , Security_Name , ClassInfo[i].Redemption.ToString("dd-MMM-yyyy"), ClassInfo[i].BaseAmount, ClassInfo[i].CumulativeWeight,0, Security_Name , WF_TYPE_ID ,"USD", IS_ILLIQUID, PositionBaseValue , SecurityPending,1, IS_Pined, IsOfficial,0, isOverRidden ,LiquidityModel,1,0,MGR_FUND_CLASS_ID});
                    if (ClassInfo[i].Notice != null && PenaltyType == "Without")
                        LiquidityTermsTable.Rows.Add(new Object[] { "t:" + APPROVAL_STATUS, WF_PORTFOLIO_ID, INSIGHT_PRODUCT_NAME.Replace("'", "''"), INSIGHT_PRODUCT_ID, EvalID, evaldate.ToString("dd-MMM-yyyy"), +ClassInfo[i].WF_SecurityID, Security_Name, ClassInfo[i].BaseAmount, ClassInfo[i].weight, 1, IS_Pined, IsOfficial, ClassInfo[i].FirstCashFlow.ToString("dd-MMM-yyyy"), ClassInfo[i].Redemption_Freq, Convert.ToInt64(ClassInfo[i].Notice), ClassInfo[i].penalty1weight, ClassInfo[i].penalty2weight, ClassInfo[i].penalty3weight,1- ClassInfo[i].Gate, ClassInfo[i].LockupEndDate.ToString("dd-MMM-yyyy"), ClassInfo[i].RedNoticeDue.ToString("dd-MMM-yyyy") });
                }

            }
            catch (Exception e) {
                LogManager.GetLogger("General").Error("Error while Inserting in DB for Product :" + Productname +" Error:" + e);
                log.Info("Error while Inserting in DB for Product :" + Productname + " Error:" + e);
            }

         


        }
        private static void GetData(DataSet ds, string Query)
        {
            using (var conn = new OracleConnection(ConfigurationManager.ConnectionStrings["mgr"].ConnectionString))
            {
                conn.Open();


                var com = new OracleCommand(Query);
                com.Connection = conn;
                com.CommandType = CommandType.Text;

                var da = new OracleDataAdapter();
                da.SelectCommand = com;

                da.Fill(ds);
                conn.Close();
            }
        }
        private static IEnumerable<DateTime> QuartersInYear(int year)
        {
            return new List<DateTime>() {
                    new DateTime(year, 3, 31),
                    new DateTime(year, 6, 30),
                    new DateTime(year, 9, 30),
                    new DateTime(year, 12, 31),
                };
        }
        public static DateTime EndOfMonth(DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month, 1).AddMonths(1).AddDays(-1);
        }
        public static DateTime NearestQuarterEnd(DateTime date, bool flag)
        {
            if (flag == true)
            {
                if (date.Month % 3 != 0)
                {
                    IEnumerable<DateTime> candidates =
                      QuartersInYear(date.Year).Union(QuartersInYear(date.Year - 1));
                    return candidates.Where(d => d >= date).OrderBy(d => d).First();
                }
                else
                    return EndOfMonth(date);
            }
            else
            {
                return EndOfMonth(date);
            }
        }
        private static void GetInvestmentDetail(int Product_ID, DataSet ds, DateTime Period)
        {
            try
            {
                using (var conn = new OracleConnection(ConfigurationManager.ConnectionStrings["mgr"].ConnectionString))
                {
                    DateTime BegPeriod = new DateTime(Period.Year, Period.Month, 1);
                    DateTime EndPeriod = BegPeriod.AddMonths(1).AddDays(-1);
                    conn.Open();
                    DataSet DSPeriod = new DataSet();

                    string Query = "Select max(Eval_Date) from MIS_PERMAL.Eval_Holdings WHERE Insight_Product_ID =" + Product_ID + "  and Eval_Date <='" + Period.ToString("dd-MMM-yyyy") + "' order by eval_Date";
                    var com = new OracleCommand(Query);
                    com.Connection = conn;
                    com.CommandType = CommandType.Text;

                    var da = new OracleDataAdapter();
                    da.SelectCommand = com;

                    da.Fill(DSPeriod);
                    if (Convert.ToDateTime(DSPeriod.Tables[0].Rows[0][0].ToString()) < Period)
                    {
                        BegPeriod = Convert.ToDateTime(DSPeriod.Tables[0].Rows[0][0].ToString());
                    }
                    else
                    {
                        BegPeriod = Period;
                    }

                    Query = "select * from ( " +
                                       "SELECT MGR.FUND_Class.ID,Insight.Product.Name,evalholding.MGR_FUND_NAME,MGR.FUND_Class.Description,MGR.FUND_Class.LEGAL_FUND_CLASS_ID,pa.portfolio AS WF_PORTFOLIO_ID, pa.paseedid,poa.security, pa.padate,EvalHolding.WF_Security_Name, sec.securityseedid AS WF_SECURITY_ID,Round(poa.wEIGHT,10) as Weight, Case When POA.INTINVESTDATE IS NULL then  POA.OPENDATE else POA.INTINVESTDATE end as INITIAL_INvestment_Date,POA.BASEVALUE +POA.EqualizationFACTOR AS emv,b.FinalAMT,EVALHOLDING.INSIGHT_PRODUCT_ID,MGR.FUND_CLASS.IS_SIDE_POCKET" +
                                   ",RANK () OVER (" +
                                   "PARTITION BY  portfolio, padate ORDER BY internalcode DESC, paseedid DESC) R " +
                                   "FROM WFPROD_PERMAL.PORTFOLIOARREST pa " +
                                   "LEFT JOIN  WFPROD_PERMAL.ACCOUNTARREST aa " +
                                   "ON AA.PORTFOLIOARREST= PA.PASEEDID " +
                                   "LEFT JOIN WFPROD_PERMAL.POSITIONARREST poa " +
                                   "ON POA.ACCOUNTARREST=aa.aaseedid " +
                                   "LEFT JOIN MIS_PERMAL.MV_WF_SECURITY sec " +
                                   "ON SEC.SECURITYSEEDID=poa.security " +
                                   "Join (select TransactionSEEDID,Operation,VALUEDATE from WFPROD_PERMAL.Transaction_ ) tran " +
                                   " on Tran.TransactionSEEDID = POA.OPENTRANSKEY Join " +
                                   " (Select OperationSeedID,FinalAMT from  WFPROD_PERMAL.OPERATIon) b " +
                                   " on tran.Operation = b.OperationSeedID " +
                                   " join ( Select * from MIS_PERMAL.Eval_Holdings where eval_Date =(Select Max(eval_Date) from  MIS_PERMAL.Eval_Holdings where eval_date <='" + BegPeriod.ToString("dd-MMM-yyyy") + "'))  EvalHolding " +
                                   " on EvalHolding.WF_PORTFOLIO_ID = pa.portfolio and sec.securityseedid = EvalHolding.WF_SEcurity_ID " +
                                   " Join MGR.FUND_Class on MGR.FUND_Class.ID = EvalHolding.MGR_FUND_CLASS_ID " +
                                   " Join Insight.Product on EvalHolding.Insight_Product_ID = Insight.Product.ID " +
                                   " WHERE EvalHolding.Insight_Product_ID =" + Product_ID + " AND padate='" + BegPeriod.ToString("dd-MMM-yyyy") + "' AND internalcode IN ('EOM', 'EOD') " +
                                   " )where r=1 and EMV>0 order by MGR_FUND_name ";
                    com = new OracleCommand(Query);
                    com.Connection = conn;
                    com.CommandType = CommandType.Text;

                    da = new OracleDataAdapter();
                    da.SelectCommand = com;

                    da.Fill(ds);

                }
            }
            catch (Exception e)
            {
                LogManager.GetLogger("General").Error("Error while Getting Investment Detail for :" + Product_ID + " Error:" + e);
                log.Info("Error while Getting Investment Detail for :" + Product_ID + " Error:" + e);
            }
        }
        private static double[] GetCash(DateTime Period, int Product)
        {
            using (var conn = new OracleConnection(ConfigurationManager.ConnectionStrings["mgr"].ConnectionString))
            {
                DateTime BegPeriod = new DateTime(Period.Year, Period.Month, 1); // Getting the Begginning Month Date
                DateTime EndPeriod = new DateTime(Period.Year, Period.Month, 1).AddMonths(1).AddDays(-1);// Getting the EndDat of the month
                conn.Open();
                DataSet ds = new DataSet();
                double [] cash  = new double[2];
                //string Query = "select Sum(BOM_VAlue) as MVAL, Round(Sum(BOM_Weight),5) as Weight from MIS_PERMAL.V_EVAL_PORTFOLIO_WEIGHT_CASH where Product_ID = '" + Product + "' and  price_Date = '" + BegPeriod.ToString("dd-MMM-yyyy") + "' and fund_ID in (9999,9998) ";
                //string Query = "Select SUM(Mval) as MVAL from ( select Sum(BOM_VAlue) as MVAL from MIS_PERMAL.V_EVAL_PORTFOLIO_WEIGHT_CASH where Product_ID ="+ Product +" and  price_Date ='"+BegPeriod.ToString("dd-MMM-yyyy") +"'  and fund_ID in (9999,9998)" +
                //                " union all select -Sum(PortfolioAmount) as MVAL from mis_permal.v_operation_transactions where insight_product_id="+Product+" and transaction_valuedate>='"+Period.ToString("dd-MMM-yyyy")+"' and operation_Valuedate <'"+EndPeriod.ToString("dd-MMM-yyyy")+"')";
                string Query = "Select AUM_NET from MIS_Permal.Eval_portfolio where insight_product_id=" + Product + " and eval_Date = '" + EndPeriod.ToString("dd-MMM-yyyy") + "'"; // Returning the Product AUM
                OracleCommand com = new OracleCommand(Query);
                com.Connection = conn;
                com.CommandType = CommandType.Text;
                OracleDataReader reader = com.ExecuteReader();
                while (reader.Read())
                {
                    cash[0] = Convert.ToDouble(reader[0]);
                    cash[1] = 0;
                }
                if (reader.HasRows == false)
                {
                    Query = "Select AUM_NET from MIS_Permal.Eval_portfolio where insight_product_id=" + Product + " and eval_Date = '" + BegPeriod.ToString("dd-MMM-yyyy") + "'"; // Returning the Product AUM
                     com = new OracleCommand(Query);
                    com.Connection = conn;
                    com.CommandType = CommandType.Text;
                   
                     reader = com.ExecuteReader();
                    while (reader.Read())
                    {
                        cash[0] = Convert.ToDouble(reader[0]);
                        cash[1] = 0;
                    }
                }
                if (reader.HasRows == false)
                {
                    Query = "Select AUM_NET from MIS_Permal.Eval_portfolio where insight_product_id=" + Product + " and eval_Date = '" + BegPeriod.AddDays(-1).ToString("dd-MMM-yyyy") + "'"; // Returning the Product AUM
                    com = new OracleCommand(Query);
                    com.Connection = conn;
                    com.CommandType = CommandType.Text;

                    reader = com.ExecuteReader();
                    while (reader.Read())
                    {
                        cash[0] = Convert.ToDouble(reader[0]);
                        cash[1] = 0;
                    }
                }
                return cash;
            }
        }
        private static DataSet GetRedemptions(DateTime Period, int Product)
        {
            using (var conn = new OracleConnection(ConfigurationManager.ConnectionStrings["mgr"].ConnectionString))
            {
                DateTime EndPeriod = new DateTime(Period.Year, Period.Month, 1).AddMonths(1).AddDays(-1);
                conn.Open();
                DataSet ds = new DataSet();
                string Query = "select * from mis_permal.v_operation_transactions where insight_product_id=" + Product + " and transaction_valuedate>='" + Period.ToString("dd-MMM-yyyy") + "' and operation_Valuedate <'" + EndPeriod.ToString("dd-MMM-yyyy") + "' and TYPE_='RED'";
               
                GetData(ds, Query);
                return ds;
            }
        }
        private static DataSet GetPendingSecurities(DateTime Period, int Product)
        {
            using (var conn = new OracleConnection(ConfigurationManager.ConnectionStrings["mgr"].ConnectionString))
            {
                DateTime EndPeriod = new DateTime(Period.Year, Period.Month, 1).AddMonths(1).AddDays(-1);
                conn.Open();
                DataSet ds = new DataSet();
                string Query = "Select a.*,MGR.FUND_CLASS.ID,MGR.FUND.Name,MGR.FUND_Class.Description,MGR.FUND_Class.LEGAL_FUND_CLASS_ID,MGR.FUND_CLASS.IS_SIDE_POCKET from (select Insight_Product_ID,security as WF_SECURITY_ID, Security_Name,Operation_Valuedate,portfolioamount,MGR_FUND_ID,MGR_CLASS_ID  from mis_permal.v_operation_transactions " +
                                "where operation_valuedate>='" + EndPeriod.ToString("dd-MMM-yyyy") + "' and  transaction_valuedate<='" + EndPeriod.ToString("dd-MMM-yyyy") + "' and TYPE_='SUB') a " +
                                "join  MGR.FUND_Class " +
                                "on MGR.FUND_CLASS.ID = a.MGR_CLASS_ID " +
                                "join MGR.FUND " +
                                "on MGR.FUND.ID = a.MGR_FUND_ID "  +
                                "Where Insight_Product_ID =" + Product;

                GetData(ds, Query);
                return ds;
            }
        }
            
        private static void GetLiquidityterms(int Legal_Class_ID, DataSet ds_LiquidityTerms)
        {

            string Query;
            int classID;
            var ds = new DataSet();
            Query = "Select id from MGR.LEgal_Fund_Class where Legal_Class_Parent_ID = " + Legal_Class_ID;
            GetData(ds, Query);
            if (ds.Tables[0].Rows.Count > 0)
                classID = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
            else
                classID = Legal_Class_ID;
            try
            {
                Query = "Select Fund_ID,Description,Rolling_Lock_up_Terms,NAME as Reds_Frqncy,NVL(REDS_NOTICE_DAYS,0) as REDS_NOTICE_DAYS,NVL(LOCK_UP_FEES_0_12_IN_PCT,0) as LOCK_UP_FEES_0_12_IN_PCT, " +
                    "NVL(LOCK_UP_FEES_12_24_IN_PCT,0) as LOCK_UP_FEES_12_24_IN_PCT, NVL(LOCK_UP_FEES_24_36_IN_PCT,0) as LOCK_UP_FEES_24_36_IN_PCT,NVL(HardLockDuration,0) as HardLockDuration,NVL(SoftLockDuration,0) as SoftLockDuration,NVL(Gate_Investor_PCT,100) as Gate_Investor_PCT ,NVL(SIDE_POCKET_PROVISION,0) as SIDE_POCKET_PROVISION ,NVL(LockupType,6) as LockupType,Apply_Gate_Declining_Balance,NVL(NOtice_TYPE_OF_Days_ID,2) as Notice_Type  from (" +
                    "(Select * from mgr.Legal_Fund_Class where id  =" + classID + " ) b " +
                    "Join MGR.LIQ_REDS_FRQNCY on b.REDS_FRQNCY_ID  = MGR.LIQ_REDS_FRQNCY.ID Left Join (Select ID,Name as LockupType from MGR.LIQ_LOCKUP_TYPE) c on b.Lockup_type_id = c.ID " +
                    "left join (Select MGR.LIQ_DURATION_MONTHS.NAME as HardLockDuration,MGR.LIQ_DURATION_MONTHS.ID from  Mgr.LIQ_Duration_Months) a " +
                    "on b.HARD_DURATION_MONTHS_ID     = a.ID Left Join (Select MGR.LIQ_DURATION_MONTHS.NAME as SoftLockDuration,MGR.LIQ_DURATION_MONTHS.ID from  Mgr.LIQ_Duration_Months) a on b.Soft_DURATION_MONTHS_ID     = a.ID )";

                GetData(ds_LiquidityTerms, Query);
            }
            catch (Exception e)
            {
                LogManager.GetLogger("General").Error("Error while Getting Liquidity Terms :" + Legal_Class_ID + " Error:" + e);
                log.Info("Error while Getting Liquidity Terms :" + Legal_Class_ID + " Error:" + e);
            }
        }
        private static void liquidity_Calculate(int Product_ID, Dictionary<int, List<LiqStruct>> WithoutPenalty, Dictionary<int, List<LiqStruct>> WithPenalty, DateTime Period, int EvalID)
        {
            var ds_LiquidityTerms = new DataSet();
            var InvestmentDetail = new DataSet();
            GetInvestmentDetail(Product_ID, InvestmentDetail,Period);
            int ClassID, Notice, HardlockDuration, SoftLockDuration;
            string Reds_FRQNCY;
            bool SidePocket;
            double EMV, Weight;
            double Gate, Fees0_12, Fees12_24, Fees24_36;
            string lockupType;
            string Manager="";
            int MGR_FUND_CLASS_ID;
            string MGR_FUND_ID;
            string WF_SECURITY_NAME;
            int WF_PortID = 0;
            int NoticeType = 0;
            var PortfolioBalance = InvestmentDetail.Tables[0].AsEnumerable().Sum(x => x.Field<Decimal>("EMV")).ToString();
            int WF_SecurityID, DecliningBalanceGate=0;
            bool check = false;
            List<LiqStruct> Hold = new List<LiqStruct>();
            List<LiqStruct> Holdtemp = new List<LiqStruct>();
            LiqStruct temp = new LiqStruct();

            // Total Portfolio AUM
            double[] cash = new double[2];
            cash = GetCash(Period, Product_ID);
            Double TotalPortfolioWithCash = cash[0];


            //Dictionary<int,List<LiqStruct>> WithoutPenalty = new  Dictionary<int,List<LiqStruct>>();
            for (var i = 0; i < InvestmentDetail.Tables[0].Rows.Count; i++)
            {
                try
                {
                    ClassID = Convert.ToInt32(InvestmentDetail.Tables[0].Rows[i]["LEGAL_FUND_CLASS_ID"]);
                    Manager = Convert.ToString(InvestmentDetail.Tables[0].Rows[i]["MGR_FUND_NAME"]);
                    WF_PortID = Convert.ToInt32(InvestmentDetail.Tables[0].Rows[i]["WF_PORTFOLIO_ID"]);
                    WF_SecurityID = Convert.ToInt32(InvestmentDetail.Tables[0].Rows[i]["WF_SECURITY_ID"]);
                    MGR_FUND_CLASS_ID = Convert.ToInt32(InvestmentDetail.Tables[0].Rows[i]["ID"]);
                    ds_LiquidityTerms = new DataSet();
                    // Get the Liquidity Terms for the Share class
                    GetLiquidityterms(ClassID, ds_LiquidityTerms);
                    ///////////////////////////////////////////

                    if (ds_LiquidityTerms.Tables[0].Rows.Count == 0)
                    {

                        LogManager.GetLogger("General").Error("Liquidity Terms Missing for Manager :" + Manager + " Class ID :" + ClassID);
                        log.Info("Liquidity Terms Missing for Manager :" + Manager + " Class ID :" + ClassID);
                        return;
                    }

                    if (ds_LiquidityTerms.Tables[0].Rows[0]["REDS_NOTICE_DAYS"] == null)
                    {
                        LogManager.GetLogger("General").Error("Notice Days Missing for Manager :" + Manager + " Class ID :" + ClassID);
                        log.Info("Notice Days Missing for Manager :" + Manager + " Class ID :" + ClassID);
                        return;
                    }
                    Notice = Convert.ToInt32(ds_LiquidityTerms.Tables[0].Rows[0]["REDS_NOTICE_DAYS"]);
                    if (ds_LiquidityTerms.Tables[0].Rows[0]["Reds_Frqncy"] == null)
                    {
                        LogManager.GetLogger("General").Error("Redemption Frequency Missing for Manager :" + Manager + " Class ID :" + ClassID);
                        log.Info("Redemption Frequency Missing for Manager :" + Manager + " Class ID :" + ClassID);
                        return;
                    }
                    Reds_FRQNCY = Convert.ToString(ds_LiquidityTerms.Tables[0].Rows[0]["Reds_Frqncy"]);

                    Gate = Convert.ToDouble(ds_LiquidityTerms.Tables[0].Rows[0]["Gate_Investor_PCT"]);
                    DecliningBalanceGate = Convert.ToInt32(ds_LiquidityTerms.Tables[0].Rows[0]["Apply_Gate_Declining_Balance"]);
                    if (Gate == 0)
                        Gate = 100;
                    SidePocket = Convert.ToBoolean(InvestmentDetail.Tables[0].Rows[i]["IS_SIDE_POCKET"]);
                    if (Convert.ToString(ds_LiquidityTerms.Tables[0].Rows[0]["HardLockDuration"]) == "N/A")
                        HardlockDuration = 0;
                    else
                        HardlockDuration = Convert.ToInt32(ds_LiquidityTerms.Tables[0].Rows[0]["HardLockDuration"]);
                    if (Convert.ToString(ds_LiquidityTerms.Tables[0].Rows[0]["SoftLockDuration"]) == "N/A")
                    {
                        SoftLockDuration = 0;
                    }
                    else
                        SoftLockDuration = Convert.ToInt32(ds_LiquidityTerms.Tables[0].Rows[0]["SoftLockDuration"]);
                    NoticeType = Convert.ToInt32(ds_LiquidityTerms.Tables[0].Rows[0]["Notice_Type"]);
                    Fees0_12 = Convert.ToDouble(ds_LiquidityTerms.Tables[0].Rows[0]["LOCK_UP_FEES_0_12_IN_PCT"]);
                    Fees12_24 = Convert.ToDouble(ds_LiquidityTerms.Tables[0].Rows[0]["LOCK_UP_FEES_12_24_IN_PCT"]);
                    Fees24_36 = Convert.ToDouble(ds_LiquidityTerms.Tables[0].Rows[0]["LOCK_UP_FEES_24_36_IN_PCT"]);
                    EMV = Convert.ToDouble(InvestmentDetail.Tables[0].Rows[i]["EMV"]);
                    Weight = Convert.ToDouble(InvestmentDetail.Tables[0].Rows[i]["Weight"]);
                    lockupType = Convert.ToString(ds_LiquidityTerms.Tables[0].Rows[0]["Lockuptype"]);
                    Hold = new List<LiqStruct>();
                    WF_SECURITY_NAME = Convert.ToString(InvestmentDetail.Tables[0].Rows[i]["WF_SECURITY_NAME"]);
                    Hold = GetLiquidityWithoutPenalty(Convert.ToDateTime(Period), Convert.ToDateTime(InvestmentDetail.Tables[0].Rows[i]["INITIAL_INvestment_Date"]), EMV, Weight, Manager, ClassID, WF_PortID, WF_SecurityID, WF_SECURITY_NAME, false, Notice, Reds_FRQNCY, Gate / 100, DecliningBalanceGate, SidePocket, lockupType, HardlockDuration, SoftLockDuration, Fees0_12, Fees12_24, Fees24_36, NoticeType);
                   
                    InsertIntoDatabase(Hold, Product_ID, Convert.ToString(InvestmentDetail.Tables[0].Rows[0]["NAME"]), "Without", Period, 0, EvalID,MGR_FUND_CLASS_ID,1);
                    Hold = new List<LiqStruct>();
                    Hold = GetLiquidityWithPenalty(Convert.ToDateTime(Period), Convert.ToDateTime(InvestmentDetail.Tables[0].Rows[i]["INITIAL_INvestment_Date"]), EMV, Weight, Manager, ClassID, WF_PortID, WF_SecurityID, WF_SECURITY_NAME, false, Notice, Reds_FRQNCY, Gate / 100, DecliningBalanceGate, SidePocket, lockupType, HardlockDuration, SoftLockDuration, Fees0_12, Fees12_24, Fees24_36, NoticeType);
                    InsertIntoDatabase(Hold, Product_ID, Convert.ToString(InvestmentDetail.Tables[0].Rows[0]["NAME"]), "With", Period, 0, EvalID, MGR_FUND_CLASS_ID,1);
                    //InsertIntoDatabase(Hold, Product_ID, Convert.ToString(InvestmentDetail.Tables[0].Rows[0]["NAME"]), "Extreme", Period, 0, EvalID, MGR_FUND_CLASS_ID);
                }
                catch (Exception e)
                {
                    LogManager.GetLogger("General").Error("Error while calculating Liquidity for :" + Product_ID + " and Manager:" + Manager +" Error:" + e);
                    log.Info("Error while calculating Liquidity for :" + Product_ID + " and Manager:" + Manager + " Error:" + e);
                }
            }

            /////////////Getting Pending Securities/////////////////////////////////
            DataSet PendingSecurity = new DataSet();
            PendingSecurity = GetPendingSecurities(Period, Product_ID);
            var PendingSecurityAUM = PendingSecurity.Tables[0].AsEnumerable().Sum(x => x.Field<Decimal>("PortfolioAmount")).ToString();
            bool checkFlag = false;
            try
            {
               
                for (int i = 0; i < PendingSecurity.Tables[0].Rows.Count; i++)
                {
                    ClassID = Convert.ToInt32(PendingSecurity.Tables[0].Rows[i]["LEGAL_FUND_CLASS_ID"]);
                    Manager = Convert.ToString(PendingSecurity.Tables[0].Rows[i]["NAME"]);
                    WF_SecurityID = Convert.ToInt32(PendingSecurity.Tables[0].Rows[i]["WF_SECURITY_ID"]);
                    SidePocket = Convert.ToBoolean(PendingSecurity.Tables[0].Rows[i]["IS_SIDE_POCKET"]);
                    MGR_FUND_CLASS_ID = Convert.ToInt32(InvestmentDetail.Tables[0].Rows[i]["ID"]);
                    ds_LiquidityTerms = new DataSet();
                    // Get the Liquidity Terms for the Share class
                    GetLiquidityterms(ClassID, ds_LiquidityTerms);
                    if (ds_LiquidityTerms.Tables[0].Rows.Count == 0)
                    {

                        LogManager.GetLogger("General").Error("Liquidity Terms Missing for Manager :" + Manager + " Class ID :" + ClassID);
                        log.Info("Liquidity Terms Missing for Manager :" + Manager + " Class ID :" + ClassID);
                        checkFlag = true;
                    }

                    else if (ds_LiquidityTerms.Tables[0].Rows[0]["REDS_NOTICE_DAYS"] == null)
                    {
                        LogManager.GetLogger("General").Error("Notice Days Missing for Manager :" + Manager + " Class ID :" + ClassID);
                        log.Info("Notice Days Missing for Manager :" + Manager + " Class ID :" + ClassID);
                        checkFlag = true;
                    }
                    else if (ds_LiquidityTerms.Tables[0].Rows[0]["Reds_Frqncy"] == null)
                    {
                        LogManager.GetLogger("General").Error("Redemption Frequency Missing for Manager :" + Manager + " Class ID :" + ClassID);
                        log.Info("Redemption Frequency Missing for Manager :" + Manager + " Class ID :" + ClassID);
                        checkFlag = true;
                    }
                    if (checkFlag == false)
                    {
                        Notice = Convert.ToInt32(ds_LiquidityTerms.Tables[0].Rows[0]["REDS_NOTICE_DAYS"]);

                        Reds_FRQNCY = Convert.ToString(ds_LiquidityTerms.Tables[0].Rows[0]["Reds_Frqncy"]);
                        Gate = Convert.ToDouble(ds_LiquidityTerms.Tables[0].Rows[0]["Gate_Investor_PCT"]);
                        if (Gate == 0)
                            Gate = 100;
                        if (Convert.ToString(ds_LiquidityTerms.Tables[0].Rows[0]["HardLockDuration"]) == "N/A")
                            HardlockDuration = 0;
                        else
                            HardlockDuration = Convert.ToInt32(ds_LiquidityTerms.Tables[0].Rows[0]["HardLockDuration"]);
                        if (Convert.ToString(ds_LiquidityTerms.Tables[0].Rows[0]["SoftLockDuration"]) == "N/A")
                        {
                            SoftLockDuration = 0;
                        }
                        else
                            SoftLockDuration = Convert.ToInt32(ds_LiquidityTerms.Tables[0].Rows[0]["SoftLockDuration"]);
                        NoticeType = Convert.ToInt32(ds_LiquidityTerms.Tables[0].Rows[0]["Notice_Type"]);
                        Fees0_12 = Convert.ToDouble(ds_LiquidityTerms.Tables[0].Rows[0]["LOCK_UP_FEES_0_12_IN_PCT"]);
                        Fees12_24 = Convert.ToDouble(ds_LiquidityTerms.Tables[0].Rows[0]["LOCK_UP_FEES_12_24_IN_PCT"]);
                        Fees24_36 = Convert.ToDouble(ds_LiquidityTerms.Tables[0].Rows[0]["LOCK_UP_FEES_24_36_IN_PCT"]);
                        EMV = Convert.ToDouble(PendingSecurity.Tables[0].Rows[i]["PortfolioAmount"]);
                        Weight = EMV / (TotalPortfolioWithCash);
                        Hold = new List<LiqStruct>();
                        lockupType = Convert.ToString(ds_LiquidityTerms.Tables[0].Rows[0]["Lockuptype"]);
                        WF_SECURITY_NAME = Convert.ToString(PendingSecurity.Tables[0].Rows[i]["SECURITY_NAME"]);
                        if (InvestmentDetail.Tables[0].Rows.Count > 0)
                        {
                            Hold = GetLiquidityWithoutPenalty(Convert.ToDateTime(Period), Convert.ToDateTime(PendingSecurity.Tables[0].Rows[i]["Operation_ValueDate"]), EMV, Weight, Manager, ClassID, WF_PortID, WF_SecurityID, WF_SECURITY_NAME, false, Notice, Reds_FRQNCY, Gate / 100, DecliningBalanceGate, SidePocket, lockupType, HardlockDuration, SoftLockDuration, Fees0_12, Fees12_24, Fees24_36, NoticeType);
                            InsertIntoDatabase(Hold, Product_ID, Convert.ToString(InvestmentDetail.Tables[0].Rows[0]["NAME"]), "Without", Period, 1, EvalID, MGR_FUND_CLASS_ID,1);
                            Hold = new List<LiqStruct>();
                            Hold = GetLiquidityWithPenalty(Convert.ToDateTime(Period), Convert.ToDateTime(PendingSecurity.Tables[0].Rows[i]["Operation_ValueDate"]), EMV, Weight, Manager, ClassID, WF_PortID, WF_SecurityID, WF_SECURITY_NAME, false, Notice, Reds_FRQNCY, Gate / 100, DecliningBalanceGate, SidePocket, lockupType, HardlockDuration, SoftLockDuration, Fees0_12, Fees12_24, Fees24_36, NoticeType);
                            InsertIntoDatabase(Hold, Product_ID, Convert.ToString(InvestmentDetail.Tables[0].Rows[0]["NAME"]), "With", Period, 1, EvalID, MGR_FUND_CLASS_ID,1);
                            //InsertIntoDatabase(Hold, Product_ID, Convert.ToString(InvestmentDetail.Tables[0].Rows[0]["NAME"]), "Extreme", Period, 1, EvalID, MGR_FUND_CLASS_ID);
                       
                        }
                    }
                }
            }

            catch (Exception e)
            {
                LogManager.GetLogger("General").Error("Error while Getting Pending Security for :" + Product_ID + " and Manager:" + Manager + " Error:" + e);
                log.Info("Error while Getting Pending Security for :" + Product_ID + " and Manager:" + Manager + " Error:" + e);
            }
                //////////////////////////////////////////////////////////////////////






                //Getting the Redemptions
                Hold = new List<LiqStruct>();
            DataSet ds = new DataSet();
            LiqStruct tempStruct = new LiqStruct();

            try
            {

                ds = GetRedemptions(Period, Product_ID);

                var RedemptionBalance = ds.Tables[0].AsEnumerable().Sum(x => x.Field<Decimal>("PORTFOLIOAMOUNT")).ToString();
                if (InvestmentDetail.Tables[0].Rows.Count > 0)
                {
                    for (int q = 0; q < ds.Tables[0].Rows.Count; q++)
                    {
                        tempStruct = new LiqStruct();
                        tempStruct.IsLiquid = 0;
                        tempStruct.ManagerName = ds.Tables[0].Rows[q]["Security_Name"].ToString();
                        tempStruct.PositionBase = Convert.ToDouble(ds.Tables[0].Rows[q]["PORTFOLIOAMOUNT"].ToString());
                        tempStruct.BaseAmount = tempStruct.PositionBase;
                        tempStruct.CumulativeWeight = tempStruct.PositionBase / TotalPortfolioWithCash;
                        tempStruct.Redemption = Convert.ToDateTime(ds.Tables[0].Rows[q]["TRANSACTION_VALUEDATE"].ToString());
                        tempStruct.WF_SECURITY_NAME = tempStruct.ManagerName;
                        tempStruct.WF_SecurityID = Convert.ToInt32(ds.Tables[0].Rows[q]["SECURITY"].ToString());
                        tempStruct.WF_PortfolioID = WF_PortID;
                        Hold.Add(tempStruct);
                        InsertIntoDatabase(Hold, Product_ID, Convert.ToString(InvestmentDetail.Tables[0].Rows[0]["NAME"]), "Without", Period, 0, EvalID,0,9);
                        InsertIntoDatabase(Hold, Product_ID, Convert.ToString(InvestmentDetail.Tables[0].Rows[0]["NAME"]), "With", Period, 0, EvalID, 0,9);
                        //InsertIntoDatabase(Hold, Product_ID, Convert.ToString(InvestmentDetail.Tables[0].Rows[0]["NAME"]), "Extreme", Period, 0, EvalID, 0);
                        Hold = new List<LiqStruct>();
                    }
                }

                //Adding Cash to LIquidity Tab;e

                Hold = new List<LiqStruct>();
                tempStruct = new LiqStruct();
                tempStruct.IsLiquid = 0;
                tempStruct.ManagerName = "Cash";
                tempStruct.PositionBase = TotalPortfolioWithCash - Convert.ToDouble(RedemptionBalance) - Convert.ToDouble(PortfolioBalance) - Convert.ToDouble(PendingSecurityAUM);
                tempStruct.BaseAmount = TotalPortfolioWithCash - Convert.ToDouble(RedemptionBalance) - Convert.ToDouble(PortfolioBalance) - Convert.ToDouble(PendingSecurityAUM);
                tempStruct.CumulativeWeight = tempStruct.PositionBase / TotalPortfolioWithCash;
                tempStruct.Redemption = Period.AddDays(1);
                tempStruct.WF_SECURITY_NAME = "Cash";
                tempStruct.WF_PortfolioID = WF_PortID;
                Hold.Add(tempStruct);
                if (InvestmentDetail.Tables[0].Rows.Count != 0)
                {
                    InsertIntoDatabase(Hold, Product_ID, Convert.ToString(InvestmentDetail.Tables[0].Rows[0]["NAME"]), "Without", Period, 0, EvalID,0,1);
                    InsertIntoDatabase(Hold, Product_ID, Convert.ToString(InvestmentDetail.Tables[0].Rows[0]["NAME"]), "With", Period, 0, EvalID,0,1);
                    //InsertIntoDatabase(Hold, Product_ID, Convert.ToString(InvestmentDetail.Tables[0].Rows[0]["NAME"]), "Extreme", Period, 0, EvalID, 0);
                }
                // InsertIntoDatabase(WithPenalty, Product_ID, Convert.ToString(InvestmentDetail.Tables[0].Rows[0]["NAME"]), "With");
            }
            catch (Exception e)
            {
                LogManager.GetLogger("General").Error( " Error:" + e);
                log.Info( " Error:" + e);
            }
            
        }
        private static DateTime GetNextRollingLockDate(DateTime LotDate, DateTime Eval_Period, int HardLockDuration, int SoftlockDuration)
        {
            DateTime RollingDate;
            RollingDate = LotDate;
            if (SoftlockDuration > 0)
            {
                while (RollingDate < Eval_Period)
                {
                    RollingDate = RollingDate.AddMonths(SoftlockDuration);
                }
                LotDate = RollingDate.AddMonths(-SoftlockDuration);

            }
            else if (HardLockDuration > 0)
            {
                while (RollingDate < Eval_Period)
                {
                    RollingDate = RollingDate.AddMonths(HardLockDuration);
                }
                LotDate = RollingDate.AddMonths(-HardLockDuration);
            }
            else
            {
                while (RollingDate < Eval_Period)
                {
                    RollingDate = RollingDate.AddMonths(SoftlockDuration + HardLockDuration);
                }
                LotDate = RollingDate.AddMonths(-(SoftlockDuration + HardLockDuration));
            }
            return LotDate;
        }
        private static double FindPenalty(DateTime LotDate, DateTime Redemption, double Fees_0_12, double fees_12_24, double fees_24_36)
        {
            if (LotDate.AddMonths(12) >= Redemption)
                return Fees_0_12/100;
            else if (LotDate.AddMonths(24) >= Redemption)
                return fees_12_24/100;
            else if (LotDate.AddMonths(36) >= Redemption)
                return fees_24_36/100;
            else
                return 0;
        }
        private static DateTime AddNoticeDays(DateTime Period, int Notice, int NoticeType)
        {
            if (NoticeType == 2)
            {
                return Period.AddDays(Notice);
            }
            else
            {
                return Period.AddBusinessDays(Notice);
            }
        }
        private static DateTime SubtractNoticeDays(DateTime Period, int Notice, int NoticeType)
        {
            if (NoticeType == 2)
            {
                return Period.AddDays(-Notice);
            }
            else
            {
                return Period.AddBusinessDays(-Notice);
            }
        }
        private static List<LiqStruct> GetLiquidityWithoutPenalty(DateTime Eval_Period, DateTime LotDate, double EMV, double weight, string ManagerName, int ClassID, int WF_PortID, int WF_SecurityID,string WF_SECURITY_NAME, bool Penalty, int Notice, string Reds_Frqncy, double Investor_Gate, int DBGate, bool SidePocket, string LockType, int HardLockDuration, int SoftlockDuration, double Fees_0_12, double fees_12_24, double fees_24_36,int Notice_Type)
        {
            DateTime NextRedemption;
            
            NextRedemption = new DateTime(Eval_Period.Year,Eval_Period.Month,1).AddMonths(1).AddDays(-1);
            bool proceed = false;
            bool ComplexTerm = false;
            double MarketVal = EMV;
            double MarketWeight = weight;
            double DBGateCount = 0;
            if (DBGate != 0)
                DBGateCount = Math.Round(1 / Investor_Gate,0);
            List<LiqStruct> ListLiquidity = new List<LiqStruct>();
            LiqStruct structLiquidity = new LiqStruct();
            TimeSpan t;
            DateTime FirstCashFlow = new DateTime();
            DateTime OrigLotDate = LotDate.AddDays(-1);
            bool CevianYear1 = false;
            bool CevianYear2 = false;
            structLiquidity.ManagerName = ManagerName;
            structLiquidity.weight = weight;
            structLiquidity.PositionBase = EMV;
            structLiquidity.IsLiquid = 0;
            structLiquidity.WF_PortfolioID = WF_PortID;
            structLiquidity.WF_SecurityID = WF_SecurityID;
            structLiquidity.WF_SECURITY_NAME = WF_SECURITY_NAME;
            structLiquidity.Redemption_Freq = Reds_Frqncy;
            structLiquidity.Gate = Investor_Gate;
            structLiquidity.penalty1weight = Fees_0_12;
            structLiquidity.penalty2weight = fees_12_24;
            structLiquidity.penalty3weight = fees_24_36;
            structLiquidity.Notice = Notice.ToString();
            try
            {
                if (Reds_Frqncy == "Illiquid" || SidePocket == true || Reds_Frqncy == "Hold To Realization")
                {
                    structLiquidity.BaseAmount = MarketVal;
                    structLiquidity.Redemption = new DateTime(1970, 1, 1);
                    structLiquidity.CumulativeWeight = MarketWeight;
                    structLiquidity.FirstCashFlow = new DateTime(1970, 1, 1);
                    if (Reds_Frqncy == "Hold To Realization")
                    {
                        structLiquidity.IsLiquid = 0;
                        structLiquidity.Redemption_Freq = "H";

                    }
                    else
                    {
                        structLiquidity.IsLiquid = 1;
                        structLiquidity.Redemption_Freq = "I";
                    }
                    ListLiquidity.Add(structLiquidity);
                }
                else
                {

                    // If Lock type is Rolling find the next date when lock up expires
                    if (LockType == "Rolling")
                    {
                        
                        LotDate = GetNextRollingLockDate(LotDate, Eval_Period, HardLockDuration, SoftlockDuration);
                        structLiquidity.LockupEndDate = LotDate;
                    }
                    else

                        structLiquidity.LockupEndDate = LotDate.AddMonths(SoftlockDuration + HardLockDuration);


                    LotDate = LotDate.AddDays(-1);

                    while (Math.Round(MarketVal, 1) > 0)
                    {
                        structLiquidity.Redemption = NextRedemption;
                        structLiquidity.ManagerName = ManagerName;
                        structLiquidity.weight = weight;
                        structLiquidity.PositionBase = EMV;
                        structLiquidity.WF_PortfolioID = WF_PortID;
                        structLiquidity.WF_SecurityID = WF_SecurityID;
                        structLiquidity.WF_SECURITY_NAME = WF_SECURITY_NAME;
                        structLiquidity.Redemption_Freq = Reds_Frqncy;
                        structLiquidity.Gate = Investor_Gate;
                        structLiquidity.penalty1weight = Fees_0_12;
                        structLiquidity.penalty2weight = fees_12_24;
                        structLiquidity.penalty3weight = fees_24_36;
                        structLiquidity.Notice = Notice.ToString();
                        if (AddNoticeDays(Eval_Period, Notice, Notice_Type) > NextRedemption)
                        // if (Eval_Period.AddDays(Notice) > NextRedemption)
                        {
                            proceed = false;
                        }
                        else if (SoftlockDuration > 0 || HardLockDuration > 0)
                        {
                            if (SoftlockDuration > 0 && HardLockDuration == 0)
                            {
                                if (Penalty)
                                    proceed = true;
                                else if (LotDate.AddMonths(SoftlockDuration) <= NextRedemption)
                                    proceed = true;
                                else
                                    proceed = false;
                            }
                            else if (HardLockDuration > 0 && SoftlockDuration == 0)
                            {
                                if (LotDate.AddMonths(HardLockDuration) <= NextRedemption)
                                    proceed = true;
                                else
                                    proceed = false;
                            }
                            else
                            {
                                if (Penalty)
                                {
                                    if (LotDate.AddMonths(HardLockDuration) <= NextRedemption)
                                        proceed = true;
                                    else
                                        proceed = false;
                                }
                                else if (LotDate.AddMonths(SoftlockDuration + HardLockDuration) <= NextRedemption)
                                    proceed = true;
                                else
                                    proceed = false;
                            }
                        }
                        else
                            proceed = true;

                        // Complex Liquidity Terms 
                        switch (ClassID)
                        {
                            case 12395:// Moore Capital 
                            case 17921:
                            case 17941:

                                if (LotDate.AddMonths(HardLockDuration) <= NextRedemption && Eval_Period.AddDays(Notice) <= NextRedemption)
                                {
                                    if (NextRedemption.Month == 2 || NextRedemption.Month == 5 || NextRedemption.Month == 8 || NextRedemption.Month == 11)
                                    {
                                        structLiquidity.Redemption = NextRedemption;
                                        structLiquidity.BaseAmount = MarketVal;
                                        structLiquidity.CumulativeWeight = weight;
                                        MarketVal = 0;
                                        proceed = true;
                                    }
                                }
                                ComplexTerm = true;

                                break;
                            case 10651:// Cevian Capital Class A
                            case 10657:
                            case 11815:

                                if ((NextRedemption.Year - OrigLotDate.Year) % 3 == 0 && NextRedemption.Month == OrigLotDate.Month)
                                {
                                    structLiquidity.Redemption = NextRedemption;
                                    structLiquidity.BaseAmount = MarketVal;
                                    structLiquidity.CumulativeWeight = weight;
                                    MarketVal = 0;
                                    proceed = true;
                                }
                                else if ((NextRedemption.Year - 1 - OrigLotDate.Year) % 3 == 0 && CevianYear1 == false)
                                {
                                    if (NextRedemption.Month == OrigLotDate.Month || NextRedemption.Month % 3 == 0)
                                    {

                                        structLiquidity.Redemption = NextRedemption;
                                        structLiquidity.BaseAmount = EMV * .05;
                                        structLiquidity.CumulativeWeight = MarketWeight * .05;
                                        MarketVal = MarketVal - EMV * .05;
                                        weight = weight - MarketWeight * .05;
                                        CevianYear1 = true;
                                        proceed = true;
                                    }
                                }
                                else if ((NextRedemption.Year - 2 - OrigLotDate.Year) % 3 == 0 && CevianYear2 == false)
                                {
                                    if (NextRedemption.Month == OrigLotDate.Month || NextRedemption.Month % 3 == 0)
                                    {

                                        structLiquidity.Redemption = NextRedemption;
                                        structLiquidity.BaseAmount = EMV * .05;
                                        structLiquidity.CumulativeWeight = MarketWeight * .05;
                                        weight = weight - MarketWeight * .05;
                                        MarketVal = MarketVal - EMV * .05;
                                        CevianYear2 = true;
                                        proceed = true;
                                    }
                                }

                                ComplexTerm = true;
                                break;
                            case 11814:// Cevian Capital Class B
                            case 10656:
                            case 10650:
                                proceed = true;
                                if (NextRedemption >= OrigLotDate.AddYears(2) && AddNoticeDays(Eval_Period, Notice, Notice_Type) < NextRedemption) // If trying to redeem during first 2 years you have to pay 4% penalty
                                {
                                    if ((NextRedemption.Year - OrigLotDate.Year) % 2 == 0 && NextRedemption.Month == OrigLotDate.Month)
                                    {
                                        structLiquidity.Redemption = NextRedemption;
                                        structLiquidity.BaseAmount = MarketVal;
                                        structLiquidity.CumulativeWeight = weight;
                                        MarketVal = 0;
                                        proceed = true;
                                    }
                                    else if ((NextRedemption.Year - 1 - OrigLotDate.Year) % 2 == 0 && CevianYear1 == false)
                                    {
                                        if (NextRedemption.Month == OrigLotDate.Month || NextRedemption.Month % 3 == 0)
                                        {

                                            structLiquidity.Redemption = NextRedemption;
                                            structLiquidity.BaseAmount = EMV * .05;
                                            structLiquidity.CumulativeWeight = MarketWeight * .05;
                                            weight = weight - MarketWeight * .05;
                                            MarketVal = MarketVal - EMV * .05;
                                            CevianYear1 = true;
                                        }
                                        proceed = true;
                                    }
                                }

                                ComplexTerm = true;
                                break;
                            case 15521:
                                ComplexTerm = true;
                                if ((NextRedemption.Year - LotDate.Year) % 2 == 0 && AddNoticeDays(Eval_Period, Notice, Notice_Type) < NextRedemption && NextRedemption.Month == LotDate.Month)
                                {
                                    structLiquidity.Redemption = NextRedemption;
                                    structLiquidity.BaseAmount = MarketVal;
                                    structLiquidity.CumulativeWeight = weight;
                                    MarketVal = 0;
                                    proceed = true;
                                }
                                else if (NextRedemption.Month % 3 == 0 && AddNoticeDays(Eval_Period, Notice, Notice_Type) < NextRedemption)
                                {
                                    structLiquidity.Redemption = NextRedemption;
                                    structLiquidity.BaseAmount = EMV*.025;
                                    structLiquidity.CumulativeWeight = MarketWeight*.025;
                                    weight = weight - MarketWeight * .025;
                                    MarketVal = MarketVal - EMV * .025;
                                    proceed = true;
                                }
                                break;

                        }

                        if (proceed)
                        {
                            if (ComplexTerm == false)
                            {
                                switch (Reds_Frqncy)
                                {
                                    case "Daily":
                                        t = NextRedemption - Eval_Period;
                                        if (t.Days > 31)
                                            structLiquidity.Redemption = NextRedemption;
                                        else
                                            structLiquidity.Redemption = AddNoticeDays(Eval_Period, Notice, Notice_Type);
                                        if (DBGate == 1) // Checking if the Gate is applied to Declining Balance
                                        {
                                            if (DBGateCount == 1) //  DBGate count  would be 1 for the last payment which would be the entire remainder Balance
                                            {
                                                structLiquidity.BaseAmount = MarketVal;  // The entire remaining Balance
                                                structLiquidity.CumulativeWeight = weight;
                                                MarketVal = 0;
                                            }
                                            else
                                            {
                                                structLiquidity.BaseAmount = MarketVal * Investor_Gate;
                                                structLiquidity.CumulativeWeight = weight * Investor_Gate;
                                                MarketVal = MarketVal - MarketVal * Investor_Gate;
                                                weight = weight - weight * Investor_Gate;

                                            }
                                            DBGateCount--;
                                        }
                                        else
                                        {
                                            structLiquidity.BaseAmount = EMV * Investor_Gate;
                                            structLiquidity.CumulativeWeight = weight * Investor_Gate;
                                            MarketVal = MarketVal - EMV * Investor_Gate;
                                        }


                                        break;
                                    case "Semi-Monthly":
                                        t = NextRedemption - Eval_Period;
                                        if (t.Days > 31)
                                            structLiquidity.Redemption = NextRedemption;
                                        else
                                        {
                                            if (AddNoticeDays(Eval_Period, Notice, Notice_Type).Day < 15)
                                                structLiquidity.Redemption = new DateTime(Eval_Period.Year, Eval_Period.Month, 15);
                                            else
                                                structLiquidity.Redemption = new DateTime(Eval_Period.Year, Eval_Period.Month, 1).AddMonths(1).AddDays(-1);
                                        }
                                        if (DBGate == 1) // Checking if the Gate is applied to Declining Balance
                                        {
                                            if (DBGateCount == 1) //  DBGate count  would be 1 for the last payment which would be the entire remainder Balance
                                            {
                                                structLiquidity.BaseAmount = MarketVal;  // The entire remaining Balance
                                                structLiquidity.CumulativeWeight = weight;
                                                MarketVal = 0;
                                            }
                                            else
                                            {
                                                structLiquidity.BaseAmount = MarketVal * Investor_Gate;
                                                structLiquidity.CumulativeWeight = weight * Investor_Gate;
                                                MarketVal = MarketVal - MarketVal * Investor_Gate;
                                                weight = weight - weight * Investor_Gate;

                                            }
                                            DBGateCount--;
                                        }
                                        else
                                        {
                                            structLiquidity.BaseAmount = EMV * Investor_Gate;
                                            structLiquidity.CumulativeWeight = weight * Investor_Gate;
                                            MarketVal = MarketVal - EMV * Investor_Gate;
                                        }
                                        break;
                                    case "Weekly":
                                        t = NextRedemption - Eval_Period;
                                        if (t.Days > 31)
                                            structLiquidity.Redemption = NextRedemption;
                                        else
                                            //structLiquidity.Redemption = Eval_Period.AddDays(7+Notice);
                                            structLiquidity.Redemption = AddNoticeDays(Eval_Period, Notice + 7, Notice_Type);
                                        if (DBGate == 1) // Checking if the Gate is applied to Declining Balance
                                        {
                                            if (DBGateCount == 1) //  DBGate count  would be 1 for the last payment which would be the entire remainder Balance
                                            {
                                                structLiquidity.BaseAmount = MarketVal;  // The entire remaining Balance
                                                structLiquidity.CumulativeWeight = weight;
                                                MarketVal = 0;
                                            }
                                            else
                                            {
                                                structLiquidity.BaseAmount = MarketVal * Investor_Gate;
                                                structLiquidity.CumulativeWeight = weight * Investor_Gate;
                                                MarketVal = MarketVal - MarketVal * Investor_Gate;
                                                weight = weight - weight * Investor_Gate;

                                            }
                                            DBGateCount--;
                                        }
                                        else
                                        {
                                            structLiquidity.BaseAmount = EMV * Investor_Gate;
                                            structLiquidity.CumulativeWeight = weight * Investor_Gate;
                                            MarketVal = MarketVal - EMV * Investor_Gate;
                                        }
                                        break;
                                    case "Monthly":

                                        structLiquidity.Redemption = NextRedemption;
                                        if (DBGate == 1) // Checking if the Gate is applied to Declining Balance
                                        {
                                            if (DBGateCount == 1) //  DBGate count  would be 1 for the last payment which would be the entire remainder Balance
                                            {
                                                structLiquidity.BaseAmount = MarketVal;  // The entire remaining Balance
                                                structLiquidity.CumulativeWeight = weight;
                                                MarketVal = 0;
                                            }
                                            else
                                            {
                                                structLiquidity.BaseAmount = MarketVal * Investor_Gate;
                                                structLiquidity.CumulativeWeight = weight * Investor_Gate;
                                                MarketVal = MarketVal - MarketVal * Investor_Gate;
                                                weight = weight - weight * Investor_Gate;

                                            }
                                            DBGateCount--;
                                        }
                                        else
                                        {
                                            structLiquidity.BaseAmount = EMV * Investor_Gate;
                                            structLiquidity.CumulativeWeight = weight * Investor_Gate;
                                            MarketVal = MarketVal - EMV * Investor_Gate;
                                        }


                                        break;
                                    case "Quarterly":
                                        if (NextRedemption.Month % 3 == 0)
                                        {
                                            if (DBGate == 1) // Checking if the Gate is applied to Declining Balance
                                            {
                                                if (DBGateCount == 1) //  DBGate count  would be 1 for the last payment which would be the entire remainder Balance
                                                {
                                                    structLiquidity.BaseAmount = MarketVal;  // The entire remaining Balance
                                                    structLiquidity.CumulativeWeight = weight;
                                                    MarketVal = 0;
                                                }
                                                else
                                                {
                                                    structLiquidity.BaseAmount = MarketVal * Investor_Gate;
                                                    structLiquidity.CumulativeWeight = weight * Investor_Gate;
                                                    MarketVal = MarketVal - MarketVal * Investor_Gate;
                                                    weight = weight - weight * Investor_Gate;

                                                }
                                                DBGateCount--;
                                            }
                                            else
                                            {
                                                structLiquidity.Redemption = NextRedemption;
                                                structLiquidity.BaseAmount = EMV * Investor_Gate;
                                                structLiquidity.CumulativeWeight = weight * Investor_Gate;
                                                MarketVal = MarketVal - EMV * Investor_Gate;
                                            }

                                        }
                                        break;
                                    case "Quarter Anniversary":

                                        if ((NextRedemption.Month - LotDate.AddDays(-1).Month) % 3 == 0) // checking for the month which is the Quarter Anniversary. ie if feb is the lot date month then 
                                        {
                                            if (DBGate == 1) // Checking if the Gate is applied to Declining Balance
                                            {
                                                if (DBGateCount == 1) //  DBGate count  would be 1 for the last payment which would be the entire remainder Balance
                                                {
                                                    structLiquidity.BaseAmount = MarketVal;  // The entire remaining Balance
                                                    structLiquidity.CumulativeWeight = weight;
                                                    MarketVal = 0;
                                                }
                                                else
                                                {
                                                    structLiquidity.BaseAmount = MarketVal * Investor_Gate;
                                                    structLiquidity.CumulativeWeight = weight * Investor_Gate;
                                                    MarketVal = MarketVal - MarketVal * Investor_Gate;
                                                    weight = weight - weight * Investor_Gate;

                                                }
                                                DBGateCount--;
                                            }
                                            else
                                            {
                                                structLiquidity.Redemption = NextRedemption;
                                                structLiquidity.BaseAmount = EMV * Investor_Gate;
                                                structLiquidity.CumulativeWeight = weight * Investor_Gate;
                                                MarketVal = MarketVal - EMV * Investor_Gate;
                                            }

                                        }
                                        break;
                                    case "Semi-Annually":


                                        if (NextRedemption.Month == 6 || NextRedemption.Month == 12)
                                        {
                                            if (DBGate == 1) // Checking if the Gate is applied to Declining Balance
                                            {
                                                if (DBGateCount == 1) //  DBGate count  would be 1 for the last payment which would be the entire remainder Balance
                                                {
                                                    structLiquidity.BaseAmount = MarketVal;  // The entire remaining Balance
                                                    structLiquidity.CumulativeWeight = weight;
                                                    MarketVal = 0;
                                                }
                                                else
                                                {
                                                    structLiquidity.BaseAmount = MarketVal * Investor_Gate;
                                                    structLiquidity.CumulativeWeight = weight * Investor_Gate;
                                                    MarketVal = MarketVal - MarketVal * Investor_Gate;
                                                    weight = weight - weight * Investor_Gate;

                                                }
                                                DBGateCount--;
                                            }
                                            else
                                            {
                                                structLiquidity.Redemption = NextRedemption;
                                                structLiquidity.BaseAmount = EMV * Investor_Gate;
                                                structLiquidity.CumulativeWeight = weight * Investor_Gate;
                                                MarketVal = MarketVal - EMV * Investor_Gate;
                                            }
                                        }


                                        break;
                                    case "Anniversary":
                                  
                                    case "Rolling Anniversary":

                                        if (LotDate.AddDays(-1).Month == NextRedemption.Month)
                                        {
                                            if (DBGate == 1) // Checking if the Gate is applied to Declining Balance
                                            {
                                                if (DBGateCount == 1) //  DBGate count  would be 1 for the last payment which would be the entire remainder Balance
                                                {
                                                    structLiquidity.BaseAmount = MarketVal;  // The entire remaining Balance
                                                    structLiquidity.CumulativeWeight = weight;
                                                    MarketVal = 0;
                                                }
                                                else
                                                {
                                                    structLiquidity.BaseAmount = MarketVal * Investor_Gate;
                                                    structLiquidity.CumulativeWeight = weight * Investor_Gate;
                                                    MarketVal = MarketVal - MarketVal * Investor_Gate;
                                                    weight = weight - weight * Investor_Gate;

                                                }
                                                DBGateCount--;
                                            }
                                            else
                                            {
                                                structLiquidity.Redemption = NextRedemption;
                                                structLiquidity.BaseAmount = EMV * Investor_Gate;
                                                structLiquidity.CumulativeWeight = weight * Investor_Gate;
                                                MarketVal = MarketVal - EMV * Investor_Gate;
                                            }
                                        }


                                        break;
                                    case "Yearly":

                                        if (NextRedemption.Month == 12)
                                        {
                                            if (DBGate == 1) // Checking if the Gate is applied to Declining Balance
                                            {
                                                if (DBGateCount == 1) //  DBGate count  would be 1 for the last payment which would be the entire remainder Balance
                                                {
                                                    structLiquidity.BaseAmount = MarketVal;  // The entire remaining Balance
                                                    structLiquidity.CumulativeWeight = weight;
                                                    MarketVal = 0;
                                                }
                                                else
                                                {
                                                    structLiquidity.BaseAmount = MarketVal * Investor_Gate;
                                                    structLiquidity.CumulativeWeight = weight * Investor_Gate;
                                                    MarketVal = MarketVal - MarketVal * Investor_Gate;
                                                    weight = weight - weight * Investor_Gate;

                                                }
                                                DBGateCount--;
                                            }
                                            else
                                            {
                                                structLiquidity.Redemption = NextRedemption;
                                                structLiquidity.BaseAmount = EMV * Investor_Gate;
                                                structLiquidity.CumulativeWeight = weight * Investor_Gate;
                                                MarketVal = MarketVal - EMV * Investor_Gate;
                                            }
                                        }


                                        break;
                                    case "Anniversary Quarter End":


                                        if (NearestQuarterEnd(LotDate.AddDays(-1), true).Month == NextRedemption.Month)
                                        {
                                            if (DBGate == 1) // Checking if the Gate is applied to Declining Balance
                                            {
                                                if (DBGateCount == 1) //  DBGate count  would be 1 for the last payment which would be the entire remainder Balance
                                                {
                                                    structLiquidity.BaseAmount = MarketVal;  // The entire remaining Balance
                                                    structLiquidity.CumulativeWeight = weight;
                                                    MarketVal = 0;
                                                }
                                                else
                                                {
                                                    structLiquidity.BaseAmount = MarketVal * Investor_Gate;
                                                    structLiquidity.CumulativeWeight = weight * Investor_Gate;
                                                    MarketVal = MarketVal - MarketVal * Investor_Gate;
                                                    weight = weight - weight * Investor_Gate;

                                                }
                                                DBGateCount--;
                                            }
                                            else
                                            {
                                                structLiquidity.Redemption = NextRedemption;
                                                structLiquidity.BaseAmount = EMV * Investor_Gate;
                                                structLiquidity.CumulativeWeight = weight * Investor_Gate;
                                                MarketVal = MarketVal - EMV * Investor_Gate;
                                            }
                                        }


                                        break;

                                }
                            }
                            ComplexTerm = false;
                            if (structLiquidity.BaseAmount != 0)
                            {
                                if (FirstCashFlow == DateTime.MinValue)
                                    FirstCashFlow = NextRedemption;
                                structLiquidity.FirstCashFlow = FirstCashFlow;
                                structLiquidity.RedNoticeDue = SubtractNoticeDays(NextRedemption, Notice, Notice_Type);
                              
                                ListLiquidity.Add(structLiquidity);
                                structLiquidity = new LiqStruct();
                                NextRedemption = new DateTime(NextRedemption.AddMonths(1).Year, NextRedemption.AddMonths(1).Month, 1).AddMonths(1).AddDays(-1);

                            }
                            else
                            {
                                structLiquidity = new LiqStruct();
                                NextRedemption = new DateTime(NextRedemption.AddMonths(1).Year, NextRedemption.AddMonths(1).Month, 1).AddMonths(1).AddDays(-1);
                            }
                        }
                        else
                        {
                            structLiquidity = new LiqStruct();
                            NextRedemption = new DateTime(NextRedemption.AddMonths(1).Year, NextRedemption.AddMonths(1).Month, 1).AddMonths(1).AddDays(-1);
                        }

                    }
                }
                
            }
            catch (Exception e)
            {
                LogManager.GetLogger("General").Error("Liquidity Without Penalty Error:" + e);
                log.Info("Liquidity Without Penalty Error:" + e);
            }
            return ListLiquidity;
        }
        private static List<LiqStruct> GetLiquidityWithPenalty   (DateTime Eval_Period, DateTime LotDate, double EMV, double weight, string ManagerName, int ClassID, int WF_PortID, int WF_SecurityID,string WF_SECURITY_NAME, bool Penalty, int Notice, string Reds_Frqncy, double Investor_Gate, int DBGate, bool SidePocket, string LockType, int HardLockDuration, int SoftlockDuration, double Fees_0_12, double fees_12_24, double fees_24_36,int Notice_Type)
        {
            DateTime NextRedemption;

            NextRedemption = new DateTime(Eval_Period.Year, Eval_Period.Month, 1).AddMonths(1).AddDays(-1);
            bool proceed = false;
            double MarketVal = EMV;
            double MarketWeight = weight;
            double DBGateCount = 0;
            DateTime OrigLotDate = LotDate;
            double ApplyPenalty;
            bool CevianYear1 = false;
            bool CevianYear2 = false;
            bool ComplexTerm = false;
            bool ProceedComplex = false;
           
            System.TimeSpan t;
            if (DBGate != 0)
                DBGateCount = 1 / Investor_Gate;
            List<LiqStruct> ListLiquidity = new List<LiqStruct>();
            LiqStruct structLiquidity = new LiqStruct();
            structLiquidity.ManagerName = ManagerName;
            structLiquidity.weight = weight;
            structLiquidity.PositionBase = EMV;
            structLiquidity.IsLiquid = 0;
            structLiquidity.WF_PortfolioID = WF_PortID;
            structLiquidity.WF_SecurityID = WF_SecurityID;
            structLiquidity.WF_SECURITY_NAME = WF_SECURITY_NAME;

           
            try
            {
                if (Reds_Frqncy == "Illiquid" || SidePocket == true || Reds_Frqncy == "Hold To Realization")
                {
                    structLiquidity.BaseAmount = MarketVal;
                    structLiquidity.CumulativeWeight = MarketWeight;
                    structLiquidity.Redemption = new DateTime(1970, 1, 1);
                    if (Reds_Frqncy == "Hold To Realization")
                        structLiquidity.IsLiquid = 1;
                    else
                        structLiquidity.IsLiquid = 1;

                    ListLiquidity.Add(structLiquidity);
                }
                else
                {

                    // If Lock type is Rolling find the next date when lock up expires
                    if (LockType == "Rolling")
                    {

                        LotDate = GetNextRollingLockDate(LotDate, Eval_Period, HardLockDuration, SoftlockDuration);
                  
                    }
                  
                    LotDate = LotDate.AddDays(-1);
                    while (Math.Round(MarketVal, 1) > 0)
                    {
                        structLiquidity.Redemption = NextRedemption;
                        structLiquidity.ManagerName = ManagerName;
                        structLiquidity.weight = weight;
                        structLiquidity.PositionBase = EMV;
                        structLiquidity.WF_PortfolioID = WF_PortID;
                        structLiquidity.WF_SecurityID = WF_SecurityID;
                        structLiquidity.WF_SECURITY_NAME = WF_SECURITY_NAME;
                        ProceedComplex = false;
                        if (AddNoticeDays(Eval_Period, Notice, Notice_Type) > NextRedemption)
                        {
                            proceed = false;
                        }
                        else if (SoftlockDuration > 0 || HardLockDuration > 0)
                        {
                            if (SoftlockDuration > 0 && HardLockDuration == 0)
                            {

                                if (LotDate.AddMonths(SoftlockDuration) <= NextRedemption)
                                    proceed = true;
                                else
                                    proceed = false;
                            }
                            else if (HardLockDuration > 0 && SoftlockDuration == 0)
                            {
                                if (LotDate.AddMonths(HardLockDuration) <= NextRedemption)
                                    proceed = true;
                                else
                                    proceed = false;
                            }
                            else
                            {
                                if (Penalty)
                                {
                                    if (LotDate.AddMonths(HardLockDuration) <= NextRedemption)
                                        proceed = true;
                                    else
                                        proceed = false;
                                }
                                else if (LotDate.AddMonths(SoftlockDuration + HardLockDuration) <= NextRedemption)
                                    proceed = true;
                                else
                                    proceed = false;
                            }
                        }
                        else
                            proceed = true;

                        switch (ClassID)
                        {
                            case 10651:// Cevian Capital Class A
                            case 10657:
                            case 11815:
                                proceed = false;
                                if ((NextRedemption.Year - OrigLotDate.AddDays(-1).Year) % 3 == 0 && NextRedemption.Month == OrigLotDate.AddDays(-1).Month)
                                {
                                    structLiquidity.Redemption = NextRedemption;
                                    structLiquidity.BaseAmount = MarketVal;
                                    structLiquidity.CumulativeWeight = weight;
                                    MarketVal = 0;
                                    ProceedComplex = true;
                                }
                                else if ((NextRedemption.Year - 1 - OrigLotDate.AddDays(-1).Year) % 3 == 0 && CevianYear1 == false)
                                {
                                    if (NextRedemption.Month == OrigLotDate.AddDays(-1).Month || NextRedemption.Month % 3 == 0)
                                    {

                                        structLiquidity.Redemption = NextRedemption;
                                        structLiquidity.BaseAmount = EMV * .05;
                                        structLiquidity.CumulativeWeight = MarketWeight * .05;
                                        weight = weight - MarketWeight * .05;
                                        MarketVal = MarketVal - EMV * .05;
                                        CevianYear1 = true;
                                        ProceedComplex = true;
                                    }
                                }
                                else if ((NextRedemption.Year - 2 - OrigLotDate.AddDays(-1).Year) % 3 == 0 && CevianYear2 == false)
                                {
                                    if (NextRedemption.Month == OrigLotDate.AddDays(-1).Month || NextRedemption.Month % 3 == 0)
                                    {

                                        structLiquidity.Redemption = NextRedemption;
                                        structLiquidity.BaseAmount = EMV * .05;
                                        structLiquidity.CumulativeWeight = weight * .05;
                                        MarketVal = MarketVal - EMV * .05;
                                        weight = weight - MarketWeight * .05;
                                        CevianYear2 = true;
                                        ProceedComplex = true;
                                    }
                                }

                                ComplexTerm = true;
                                break;
                            case 11814:// Cevian Capital Class B
                            case 10656:
                            case 10650:
                                proceed = false;
                                if (NextRedemption >= OrigLotDate.AddDays(-1).AddYears(2))
                                {
                                    if ((NextRedemption.Year - OrigLotDate.AddDays(-1).Year) % 2 == 0 && NextRedemption.Month == OrigLotDate.AddDays(-1).Month)
                                    {
                                        structLiquidity.Redemption = NextRedemption;
                                        structLiquidity.BaseAmount = MarketVal;
                                        structLiquidity.CumulativeWeight = weight;
                                        MarketVal = 0;
                                        ProceedComplex = true;
                                    }
                                    else if ((NextRedemption.Year - 1 - OrigLotDate.AddDays(-1).Year) % 2 == 0 && CevianYear1 == false)
                                    {
                                        if (NextRedemption.Month == OrigLotDate.AddDays(-1).Month || NextRedemption.Month % 3 == 0)
                                        {

                                            structLiquidity.Redemption = NextRedemption;
                                            structLiquidity.BaseAmount = EMV * .05;
                                            structLiquidity.CumulativeWeight = MarketWeight * .05;
                                            MarketVal = MarketVal - EMV * .05;
                                            weight = weight - MarketWeight * .05;
                                            CevianYear1 = true;
                                            ProceedComplex = true;
                                        }
                                    }
                                }
                                else if (NextRedemption.Month % 3 == 0)
                                {
                                    structLiquidity.Redemption = NextRedemption;
                                    structLiquidity.BaseAmount = EMV * .96;
                                    structLiquidity.CumulativeWeight = weight * .96;
                                    MarketVal = 0;
                                    ProceedComplex = true;

                                }


                                ComplexTerm = true;
                                break;
                        }

                        if (proceed && ListLiquidity.Count ==0) // check IF investment is out of lock then no need to pay Penalty
                        {
                            ListLiquidity = GetLiquidityWithoutPenalty(Eval_Period, LotDate, EMV, weight, ManagerName, ClassID, WF_PortID, WF_SecurityID, WF_SECURITY_NAME, Penalty, Notice, Reds_Frqncy, Investor_Gate, DBGate, SidePocket, LockType, HardLockDuration, SoftlockDuration, Fees_0_12, fees_12_24, fees_24_36, Notice_Type);
                            MarketVal = 0;
                        }
                        else
                        {
                            if (ComplexTerm == true && (ProceedComplex == false || Eval_Period.AddDays(Notice) > NextRedemption))
                            {

                                structLiquidity = new LiqStruct();
                                NextRedemption = EndOfMonth(NextRedemption.AddMonths(1));

                            }
                            else if (ComplexTerm == false && (Eval_Period.AddDays(Notice) > NextRedemption || LotDate.AddMonths(HardLockDuration) > NextRedemption)) // Checking the period is after Notice Period or Period is before Hard Lock
                            {
                                structLiquidity = new LiqStruct();
                                NextRedemption = EndOfMonth(NextRedemption.AddMonths(1));
                            }
                            else
                            {
                                if (ComplexTerm == false)
                                {
                                    if (proceed == true) // Lot is out of lock so no penalty
                                        ApplyPenalty = 1;
                                    else
                                        ApplyPenalty = 1 - FindPenalty(OrigLotDate.AddDays(-1), NextRedemption, Fees_0_12, fees_12_24, fees_24_36);
                                    switch (Reds_Frqncy)
                                    {
                                        case "Daily":
                                            t = NextRedemption - Eval_Period;
                                            if (t.Days > 31)
                                                structLiquidity.Redemption = NextRedemption;
                                            else
                                                structLiquidity.Redemption = AddNoticeDays(Eval_Period, Notice, Notice_Type);

                                            if (DBGate == 1) // Checking if the Gate is applied to Declining Balance
                                            {
                                                if (DBGateCount == 1) //  DBGate count  would be 1 for the last payment which would be the entire remainder Balance
                                                {
                                                    structLiquidity.BaseAmount = MarketVal;  // The entire remaining Balance
                                                    structLiquidity.CumulativeWeight = weight;
                                                    MarketVal = 0;
                                                }
                                                else
                                                {

                                                    structLiquidity.BaseAmount = MarketVal * Investor_Gate * ApplyPenalty;
                                                    structLiquidity.CumulativeWeight = weight * Investor_Gate * ApplyPenalty;
                                                    MarketVal = MarketVal - MarketVal * Investor_Gate;
                                                    weight = weight - weight * Investor_Gate;

                                                }
                                                DBGateCount--;
                                            }
                                            else
                                            {
                                                structLiquidity.BaseAmount = EMV * Investor_Gate * ApplyPenalty;
                                                structLiquidity.CumulativeWeight = weight * Investor_Gate * ApplyPenalty;
                                                MarketVal = MarketVal - EMV * Investor_Gate;
                                            }

                                            break;
                                        case "Semi-Monthly":
                                            t = NextRedemption - Eval_Period;
                                            if (t.Days > 31)
                                                structLiquidity.Redemption = NextRedemption;
                                            else
                                            {
                                                if (AddNoticeDays(Eval_Period, Notice, Notice_Type).Day < 15)
                                                    structLiquidity.Redemption = new DateTime(Eval_Period.Year, Eval_Period.Month, 15);
                                                else
                                                    structLiquidity.Redemption = new DateTime(Eval_Period.Year, Eval_Period.Month, 1).AddMonths(1).AddDays(-1);
                                            }

                                            if (DBGate == 1) // Checking if the Gate is applied to Declining Balance
                                            {
                                                if (DBGateCount == 1) //  DBGate count  would be 1 for the last payment which would be the entire remainder Balance
                                                {
                                                    structLiquidity.BaseAmount = MarketVal;  // The entire remaining Balance
                                                    structLiquidity.CumulativeWeight = weight;
                                                    MarketVal = 0;
                                                }
                                                else
                                                {

                                                    structLiquidity.BaseAmount = MarketVal * Investor_Gate * ApplyPenalty;
                                                    structLiquidity.CumulativeWeight = weight * Investor_Gate * ApplyPenalty;
                                                    MarketVal = MarketVal - MarketVal * Investor_Gate;
                                                    weight = weight - weight * Investor_Gate;

                                                }
                                                DBGateCount--;
                                            }
                                            else
                                            {
                                                structLiquidity.BaseAmount = EMV * Investor_Gate * ApplyPenalty;
                                                structLiquidity.CumulativeWeight = weight * Investor_Gate * ApplyPenalty;
                                                MarketVal = MarketVal - EMV * Investor_Gate;
                                            }

                                            break;
                                        case "Weekly":
                                            t = NextRedemption - Eval_Period;
                                            if (t.Days > 31)
                                                structLiquidity.Redemption = NextRedemption;
                                            else
                                                structLiquidity.Redemption = AddNoticeDays(Eval_Period, Notice + 7, Notice_Type);

                                            if (DBGate == 1) // Checking if the Gate is applied to Declining Balance
                                            {
                                                if (DBGateCount == 1) //  DBGate count  would be 1 for the last payment which would be the entire remainder Balance
                                                {
                                                    structLiquidity.BaseAmount = MarketVal;  // The entire remaining Balance
                                                    structLiquidity.CumulativeWeight = weight;
                                                    MarketVal = 0;
                                                }
                                                else
                                                {

                                                    structLiquidity.BaseAmount = MarketVal * Investor_Gate * ApplyPenalty;
                                                    structLiquidity.CumulativeWeight = weight * Investor_Gate * ApplyPenalty;
                                                    MarketVal = MarketVal - MarketVal * Investor_Gate;
                                                    weight = weight - weight * Investor_Gate;

                                                }
                                                DBGateCount--;
                                            }
                                            else
                                            {
                                                structLiquidity.BaseAmount = EMV * Investor_Gate * ApplyPenalty;
                                                structLiquidity.CumulativeWeight = weight * Investor_Gate * ApplyPenalty;
                                                MarketVal = MarketVal - EMV * Investor_Gate;
                                            }

                                            break;
                                        case "Monthly":
                                            structLiquidity.Redemption = NextRedemption;

                                            if (DBGate == 1) // Checking if the Gate is applied to Declining Balance
                                            {
                                                if (DBGateCount == 1) //  DBGate count  would be 1 for the last payment which would be the entire remainder Balance
                                                {
                                                    structLiquidity.BaseAmount = MarketVal;  // The entire remaining Balance
                                                    structLiquidity.CumulativeWeight = weight;
                                                    MarketVal = 0;
                                                }
                                                else
                                                {

                                                    structLiquidity.BaseAmount = MarketVal * Investor_Gate * ApplyPenalty;
                                                    structLiquidity.CumulativeWeight = weight * Investor_Gate * ApplyPenalty;
                                                    MarketVal = MarketVal - MarketVal * Investor_Gate;
                                                    weight = weight - weight * Investor_Gate;

                                                }
                                                DBGateCount--;
                                            }
                                            else
                                            {
                                                structLiquidity.BaseAmount = EMV * Investor_Gate * ApplyPenalty;
                                                structLiquidity.CumulativeWeight = weight * Investor_Gate * ApplyPenalty;
                                                MarketVal = MarketVal - EMV * Investor_Gate;
                                            }

                                            break;
                                        case "Quarterly":
                                            if (NextRedemption.Month % 3 == 0)
                                            {
                                                if (DBGate == 1) // Checking if the Gate is applied to Declining Balance
                                                {
                                                    if (DBGateCount == 1) //  DBGate count  would be 1 for the last payment which would be the entire remainder Balance
                                                    {
                                                        structLiquidity.BaseAmount = MarketVal * ApplyPenalty;  // The entire remaining Balance
                                                        structLiquidity.CumulativeWeight = weight * ApplyPenalty;
                                                        MarketVal = 0;
                                                    }
                                                    else
                                                    {
                                                        structLiquidity.BaseAmount = MarketVal * Investor_Gate * ApplyPenalty;
                                                        structLiquidity.CumulativeWeight = weight * Investor_Gate * ApplyPenalty;
                                                        MarketVal = MarketVal - MarketVal * Investor_Gate;
                                                        weight = weight - weight * Investor_Gate;

                                                    }
                                                    DBGateCount--;
                                                }
                                                else
                                                {
                                                    structLiquidity.Redemption = NextRedemption;
                                                    structLiquidity.BaseAmount = EMV * Investor_Gate * ApplyPenalty;
                                                    structLiquidity.CumulativeWeight = weight * Investor_Gate * ApplyPenalty;
                                                    MarketVal = MarketVal - EMV * Investor_Gate;
                                                }

                                            }
                                            break;
                                        case "Quarter Anniversary":
                                            if ((NextRedemption.Month - LotDate.AddDays(-1).Month) % 3 == 0) // checking for the month which is the Quarter Anniversary. ie if feb is the lot date month then 
                                            {
                                                if (DBGate == 1) // Checking if the Gate is applied to Declining Balance
                                                {
                                                    if (DBGateCount == 1) //  DBGate count  would be 1 for the last payment which would be the entire remainder Balance
                                                    {
                                                        structLiquidity.BaseAmount = MarketVal * ApplyPenalty;  // The entire remaining Balance
                                                        structLiquidity.CumulativeWeight = weight * ApplyPenalty;
                                                        MarketVal = 0;
                                                    }
                                                    else
                                                    {
                                                        structLiquidity.BaseAmount = MarketVal * Investor_Gate * ApplyPenalty;
                                                        structLiquidity.CumulativeWeight = weight * Investor_Gate * ApplyPenalty;
                                                        MarketVal = MarketVal - MarketVal * Investor_Gate;
                                                        weight = weight - weight * Investor_Gate;

                                                    }
                                                    DBGateCount--;
                                                }
                                                else
                                                {
                                                    structLiquidity.Redemption = NextRedemption;
                                                    structLiquidity.BaseAmount = EMV * Investor_Gate * ApplyPenalty;
                                                    structLiquidity.CumulativeWeight = weight * Investor_Gate * ApplyPenalty;
                                                    MarketVal = MarketVal - EMV * Investor_Gate;
                                                }

                                            }
                                            break;
                                        case "Semi-Annually":


                                            if (NextRedemption.Month == 6 || NextRedemption.Month == 12)
                                            {
                                                if (DBGate == 1) // Checking if the Gate is applied to Declining Balance
                                                {
                                                    if (DBGateCount == 1) //  DBGate count  would be 1 for the last payment which would be the entire remainder Balance
                                                    {
                                                        structLiquidity.BaseAmount = MarketVal * ApplyPenalty;  // The entire remaining Balance
                                                        structLiquidity.CumulativeWeight = weight * ApplyPenalty;
                                                        MarketVal = 0;
                                                    }
                                                    else
                                                    {
                                                        structLiquidity.BaseAmount = MarketVal * Investor_Gate * ApplyPenalty;
                                                        structLiquidity.CumulativeWeight = weight * Investor_Gate * ApplyPenalty;
                                                        MarketVal = MarketVal - MarketVal * Investor_Gate;
                                                        weight = weight - weight * Investor_Gate;

                                                    }
                                                    DBGateCount--;
                                                }
                                                else
                                                {
                                                    structLiquidity.Redemption = NextRedemption;
                                                    structLiquidity.BaseAmount = EMV * Investor_Gate * ApplyPenalty;
                                                    structLiquidity.CumulativeWeight = weight * Investor_Gate * ApplyPenalty;
                                                    MarketVal = MarketVal - EMV * Investor_Gate;
                                                }
                                            }


                                            break;
                                        case "Anniversary":
                                        case "Rolling Anniversary":
                                            if (LotDate.AddDays(-1).Month == NextRedemption.Month)
                                            {
                                                if (DBGate == 1) // Checking if the Gate is applied to Declining Balance
                                                {
                                                    if (DBGateCount == 1) //  DBGate count  would be 1 for the last payment which would be the entire remainder Balance
                                                    {
                                                        structLiquidity.BaseAmount = MarketVal * ApplyPenalty;  // The entire remaining Balance
                                                        structLiquidity.CumulativeWeight = weight * ApplyPenalty;
                                                        MarketVal = 0;
                                                    }
                                                    else
                                                    {
                                                        structLiquidity.BaseAmount = MarketVal * Investor_Gate * ApplyPenalty;
                                                        structLiquidity.CumulativeWeight = weight * Investor_Gate * ApplyPenalty;
                                                        MarketVal = MarketVal - MarketVal * Investor_Gate;
                                                        weight = weight - weight * Investor_Gate;

                                                    }
                                                    DBGateCount--;
                                                }
                                                else
                                                {
                                                    structLiquidity.Redemption = NextRedemption;
                                                    structLiquidity.BaseAmount = EMV * Investor_Gate * ApplyPenalty;
                                                    structLiquidity.CumulativeWeight = weight * Investor_Gate * ApplyPenalty;
                                                    MarketVal = MarketVal - EMV * Investor_Gate;
                                                }
                                            }


                                            break;
                                        case "Yearly":

                                            if (NextRedemption.Month == 12)
                                            {
                                                if (DBGate == 1) // Checking if the Gate is applied to Declining Balance
                                                {
                                                    if (DBGateCount == 1) //  DBGate count  would be 1 for the last payment which would be the entire remainder Balance
                                                    {
                                                        structLiquidity.BaseAmount = MarketVal * ApplyPenalty;  // The entire remaining Balance
                                                        structLiquidity.CumulativeWeight = weight * ApplyPenalty;
                                                        MarketVal = 0;
                                                    }
                                                    else
                                                    {
                                                        structLiquidity.BaseAmount = MarketVal * Investor_Gate * ApplyPenalty;
                                                        structLiquidity.CumulativeWeight = weight * Investor_Gate * ApplyPenalty;
                                                        MarketVal = MarketVal - MarketVal * Investor_Gate;
                                                        weight = weight - weight * Investor_Gate;

                                                    }
                                                    DBGateCount--;
                                                }
                                                else
                                                {
                                                    structLiquidity.Redemption = NextRedemption;
                                                    structLiquidity.BaseAmount = EMV * Investor_Gate * ApplyPenalty;
                                                    structLiquidity.CumulativeWeight = weight * Investor_Gate * ApplyPenalty;
                                                    MarketVal = MarketVal - EMV * Investor_Gate;
                                                }
                                            }


                                            break;
                                        case "Anniversary Quarter End":


                                            if (NearestQuarterEnd(LotDate.AddDays(-1), true).Month == NextRedemption.Month)
                                            {
                                                if (DBGate == 1) // Checking if the Gate is applied to Declining Balance
                                                {
                                                    if (DBGateCount == 1) //  DBGate count  would be 1 for the last payment which would be the entire remainder Balance
                                                    {
                                                        structLiquidity.BaseAmount = MarketVal * ApplyPenalty;  // The entire remaining Balance
                                                        structLiquidity.CumulativeWeight = weight * ApplyPenalty;
                                                        MarketVal = 0;
                                                    }
                                                    else
                                                    {
                                                        structLiquidity.BaseAmount = MarketVal * Investor_Gate * ApplyPenalty;
                                                        structLiquidity.CumulativeWeight = weight * Investor_Gate * ApplyPenalty;
                                                        MarketVal = MarketVal - MarketVal * Investor_Gate;
                                                        weight = weight - weight * Investor_Gate;

                                                    }
                                                    DBGateCount--;
                                                }
                                                else
                                                {
                                                    structLiquidity.Redemption = NextRedemption;
                                                    structLiquidity.BaseAmount = EMV * Investor_Gate * ApplyPenalty;
                                                    structLiquidity.CumulativeWeight = weight * Investor_Gate * ApplyPenalty;
                                                    MarketVal = MarketVal - EMV * Investor_Gate;
                                                }
                                            }


                                            break;
                                    }
                                }
                                if (structLiquidity.BaseAmount != 0)
                                {
                                 
                                    ListLiquidity.Add(structLiquidity);
                                    structLiquidity = new LiqStruct();
                                    NextRedemption = new DateTime(NextRedemption.AddMonths(1).Year, NextRedemption.AddMonths(1).Month, 1).AddMonths(1).AddDays(-1);

                                }
                                else
                                {
                                    structLiquidity = new LiqStruct();
                                    NextRedemption = new DateTime(NextRedemption.AddMonths(1).Year, NextRedemption.AddMonths(1).Month, 1).AddMonths(1).AddDays(-1);
                                }

                            }
                        }
                    }
                }
              
            }
            catch (Exception e)
            {
                LogManager.GetLogger("General").Error("Liquidity With Penalty Error:" + e);
                log.Info("Liquidity Without Penalty Error:" + e);
            }
            return ListLiquidity;
        }
        static void InsertEvalLliquidityAanalysis(DataTable dt)
        {

            string commandString = "INSERT INTO MIS_PERMAL.EVAL_LIQUIDITY_ANALYSIS (APPROVAL_STATUS, BASE_AMOUNT, CUMULATIVE_WEIGHT, CURRENCY_, EVAL_DATE, EVAL_ID, EVALUATION_RANK, INSIGHT_PRODUCT_ID, INSIGHT_PRODUCT_NAME, IS_ILLIQUID, IS_OFFICIAL, IS_OVERRIDDEN, IS_PINED, IS_RISK_DATA, IS_SECURITY_PENDING, LIQUIDITY_MODEL, MGR_CLASS_ID, MGR_FUND_ID, NAME, NEXT_REDEMPTION_DATE, POSITION, POSITIONS_TOTAL_BASE_VALUE, WEIGHT, WF_PORTFOLIO_ID, WF_SECURITY_ID, WF_SECURITY_NAME, WF_TYPE_ID) " +
                                   "VALUES (:apprvlstat,:baseamnt,:cumwhgt,:crncy,:evldt,:evlid,:evlrnk,:prdid,:prdnm,:isillqd,:isofficial,:isovrdn,:ispined,:isriskdata,:issecrtypending,:lqdtymdl,:mgrclssid,:mgrfndid,:name,:nxtrdmtdt,:position,:pstnttlbaseval,:wght,:wfportid,:wfsecid,:wfsecname,:wftpid)";

           OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["dev"].ConnectionString);
             //string _constr = "User Id=AHAIKIN;Password=ahaikin;Data Source=NYCORADEV01.etp.local/data001;Validate Connection=true";
             //OracleConnection connection = new OracleConnection(_constr);
            OracleCommand command = new OracleCommand(commandString, connection);
            command.ArrayBindCount = dt.Rows.Count;

            //apprvlstat
            OracleParameter approvalStatusParam = new OracleParameter("apprvlstat", OracleDbType.Varchar2);
            approvalStatusParam.Direction = ParameterDirection.Input;
            approvalStatusParam.Value = dt.AsEnumerable().Select(r=>r.Field<string>(0)).ToArray();
            command.Parameters.Add(approvalStatusParam);

            //baseamnt
            OracleParameter baseAmountParam = new OracleParameter("baseamnt", OracleDbType.Decimal);
            baseAmountParam.Direction = ParameterDirection.Input;
            baseAmountParam.Value = dt.AsEnumerable().Select(r=>r.Field<double>("BASE_AMOUNT")).ToArray();
            command.Parameters.Add(baseAmountParam);

            //cumwhgt
            OracleParameter cumulativeWeightParam = new OracleParameter("cumwhgt", OracleDbType.Decimal);
            cumulativeWeightParam.Direction = ParameterDirection.Input;
            cumulativeWeightParam.Value =dt.AsEnumerable().Select(r=>r.Field<double>("CUMULATIVE_WEIGHT")).ToArray();
            command.Parameters.Add(cumulativeWeightParam);

            //crncy
            OracleParameter currencyParam = new OracleParameter("crncy", OracleDbType.Varchar2);
            currencyParam.Direction = ParameterDirection.Input;
            currencyParam.Value = dt.AsEnumerable().Select(r=>r.Field<string>("CURRENCY_")).ToArray();
            command.Parameters.Add(currencyParam);

            //evldt
            OracleParameter evalDateParam = new OracleParameter("evldt", OracleDbType.Date);
            evalDateParam.Direction = ParameterDirection.Input;
            evalDateParam.Value = dt.AsEnumerable().Select(r=>r.Field<DateTime>("EVAL_DATE")).ToArray();
            command.Parameters.Add(evalDateParam);

            //evlid
            OracleParameter evalIdParam = new OracleParameter("evlid", OracleDbType.Int32);
            evalIdParam.Direction = ParameterDirection.Input;
            evalIdParam.Value =dt.AsEnumerable().Select(r=>r.Field<int>("EVAL_ID")).ToArray();
            command.Parameters.Add(evalIdParam);

            //evlrnk
            OracleParameter evaluationRankParam = new OracleParameter("evlrnk", OracleDbType.Int32);
            evaluationRankParam.Direction = ParameterDirection.Input;
            evaluationRankParam.Value = dt.AsEnumerable().Select(r=>r.Field<int>("EVALUATION_RANK")).ToArray();
            command.Parameters.Add(evaluationRankParam);

            //prdid
            OracleParameter insightProductIdParam = new OracleParameter("prdid", OracleDbType.Int32);
            insightProductIdParam.Direction = ParameterDirection.Input;
            insightProductIdParam.Value = dt.AsEnumerable().Select(r=>r.Field<int>("INSIGHT_PRODUCT_ID")).ToArray();
            command.Parameters.Add(insightProductIdParam);

            //prdnm
            OracleParameter insightProductNameParam = new OracleParameter("prdnm", OracleDbType.Varchar2);
            insightProductNameParam.Direction = ParameterDirection.Input;
            insightProductNameParam.Value =  dt.AsEnumerable().Select(r=>r.Field<string>("INSIGHT_PRODUCT_NAME")).ToArray();
            command.Parameters.Add(insightProductNameParam);

            //isillqd
            OracleParameter isIlliquidParam = new OracleParameter("isillqd", OracleDbType.Int32);
            isIlliquidParam.Direction = ParameterDirection.Input;
            isIlliquidParam.Value = dt.AsEnumerable().Select(r=>r.Field<int>("IS_ILLIQUID")).ToArray();
            command.Parameters.Add(isIlliquidParam);

            //isofficial
            OracleParameter isOfficialParam = new OracleParameter("isofficial", OracleDbType.Int32);
            isOfficialParam.Direction = ParameterDirection.Input;
            isOfficialParam.Value = dt.AsEnumerable().Select(r=>r.Field<int>("IS_OFFICIAL")).ToArray();
            command.Parameters.Add(isOfficialParam);

            //isovrdn
            OracleParameter isOverridenParam = new OracleParameter("isovrdn", OracleDbType.Int32);
            isOverridenParam.Direction = ParameterDirection.Input;
            isOverridenParam.Value = dt.AsEnumerable().Select(r=>r.Field<int>("IS_OVERRIDEN")).ToArray();
            command.Parameters.Add(isOverridenParam);

            //ispined
            OracleParameter isPinedParam = new OracleParameter("ispined", OracleDbType.Int32);
            isPinedParam.Direction = ParameterDirection.Input;
            isPinedParam.Value = dt.AsEnumerable().Select(r=>r.Field<int>("IS_PINED")).ToArray();
            command.Parameters.Add(isPinedParam);

            //isriskdata
            OracleParameter isRiskDataParam = new OracleParameter("isriskdata", OracleDbType.Int32);
            isRiskDataParam.Direction = ParameterDirection.Input;
            isRiskDataParam.Value = dt.AsEnumerable().Select(r=>r.Field<int>("IS_RISK_DATA")).ToArray();
            command.Parameters.Add(isRiskDataParam);

            //issecrtypending
            OracleParameter isSecurityPendingParam = new OracleParameter("issecrtypending", OracleDbType.Int32);
            isSecurityPendingParam.Direction = ParameterDirection.Input;
            isSecurityPendingParam.Value =  dt.AsEnumerable().Select(r=>r.Field<int>("IS_SECURITY_PENDING")).ToArray();
            command.Parameters.Add(isSecurityPendingParam);

            //lqdtymdl
            OracleParameter liquidityModelParam = new OracleParameter("lqdtymdl", OracleDbType.Int32);
            liquidityModelParam.Direction = ParameterDirection.Input;
            liquidityModelParam.Value =dt.AsEnumerable().Select(r=>r.Field<int>("LIQUIDITY_MODEL")).ToArray();
            command.Parameters.Add(liquidityModelParam);

            //mgrclssid
            OracleParameter mgrClassIdParam = new OracleParameter("mgrclssid", OracleDbType.Int32);
            mgrClassIdParam.Direction = ParameterDirection.Input;
            mgrClassIdParam.Value = dt.AsEnumerable().Select(r=>r.Field<int>("MGR_CLASS_ID")).ToArray();
            command.Parameters.Add(mgrClassIdParam);

            //mgrfndid
            OracleParameter mgrFundIdParam = new OracleParameter("mgrfndid", OracleDbType.Int32);
            mgrFundIdParam.Direction = ParameterDirection.Input;
            mgrFundIdParam.Value =dt.AsEnumerable().Select(r=>r.Field<int>("MGR_FUND_ID")).ToArray();
            command.Parameters.Add(mgrFundIdParam);

            //name
            OracleParameter nameParam = new OracleParameter("name", OracleDbType.Varchar2);
            nameParam.Direction = ParameterDirection.Input;
            nameParam.Value = dt.AsEnumerable().Select(r=>r.Field<string>("NAME")).ToArray();
            command.Parameters.Add(nameParam);

            //nxtrdmtdt
            OracleParameter nextRedemptionDateParam = new OracleParameter("nxtrdmtdt", OracleDbType.Date);
            nextRedemptionDateParam.Direction = ParameterDirection.Input;
            nextRedemptionDateParam.Value =dt.AsEnumerable().Select(r=>r.Field<DateTime>("NEXT_REDEMPTION_DATE")).ToArray();
            command.Parameters.Add(nextRedemptionDateParam);

            //position
            OracleParameter positionParam = new OracleParameter("position", OracleDbType.Varchar2);
            positionParam.Direction = ParameterDirection.Input;
            positionParam.Value =dt.AsEnumerable().Select(r=>r.Field<string>("POSITION")).ToArray();
            command.Parameters.Add(positionParam);

            //pstnttlbaseval
            OracleParameter positionTotalBaseValueParam = new OracleParameter("pstnttlbaseval", OracleDbType.Decimal);
            positionTotalBaseValueParam.Direction = ParameterDirection.Input;
            positionTotalBaseValueParam.Value = dt.AsEnumerable().Select(r=>r.Field<double>("POSITION_TOTAL_BASE_VALUE")).ToArray();
            command.Parameters.Add(positionTotalBaseValueParam);

            //wght
            OracleParameter weightParam = new OracleParameter("wght", OracleDbType.Decimal);
            weightParam.Direction = ParameterDirection.Input;
            weightParam.Value =dt.AsEnumerable().Select(r=>r.Field<double>("WEIGHT")).ToArray();
            command.Parameters.Add(weightParam);

            //wfportid
            OracleParameter wfPortfolioIdParam = new OracleParameter("wfportid", OracleDbType.Int32);
            wfPortfolioIdParam.Direction = ParameterDirection.Input;
            wfPortfolioIdParam.Value = dt.AsEnumerable().Select(r=>r.Field<int>("WF_PORTFOLIO_ID")).ToArray();
            command.Parameters.Add(wfPortfolioIdParam);

            //wfsecid
            OracleParameter wfSecurityIdParam = new OracleParameter("wfsecid", OracleDbType.Int32);
            wfSecurityIdParam.Direction = ParameterDirection.Input;
            wfSecurityIdParam.Value =  dt.AsEnumerable().Select(r=>r.Field<int>("WF_SECURITY_ID")).ToArray();
            command.Parameters.Add(wfSecurityIdParam);

            //wfsecname
            OracleParameter wfSecurityNameParam = new OracleParameter("wfsecname", OracleDbType.Varchar2);
            wfSecurityNameParam.Direction = ParameterDirection.Input;
            wfSecurityNameParam.Value = dt.AsEnumerable().Select(r=>r.Field<string>("WF_SECURITY_NAME")).ToArray();
            command.Parameters.Add(wfSecurityNameParam);

            //wftpid
            OracleParameter wfTypeIdParam = new OracleParameter("wftpid", OracleDbType.Int32);
            wfTypeIdParam.Direction = ParameterDirection.Input;
            wfTypeIdParam.Value =  dt.AsEnumerable().Select(r=>r.Field<int>("WF_TYPE_ID")).ToArray();
            command.Parameters.Add(wfTypeIdParam);


            try
            {
                connection.Open();
                command.ExecuteNonQuery();
               
            }
            catch (Exception ex)
            {
               Console.WriteLine("aslfhklaf");
            }
            finally
            { 
                connection.Close();
                command.Dispose();
                connection.Dispose();

            }
        }
        static void InsertEvalLliquidityTerms(DataTable dt)
        {

            string commandString = "INSERT INTO MIS_PERMAL.EVAL_LIQUIDITY_TERMS (APPROVAL_STATUS, BASE_AMOUNT, EVAL_DATE, EVAL_ID, EVALUATION_RANK, INSIGHT_PRODUCT_ID, INSIGHT_PRODUCT_NAME, IS_OFFICIAL, IS_PINED, WEIGHT, WF_SECURITY_ID, WF_SECURITY_NAME,REDEMPTION_FREQUENCY,REDEMPTION_NOTICE) " +
                                   "VALUES (:apprvlstat,:baseamnt,:evldt,:evlid,:evlrnk,:prdid,:prdnm,:isofficial,:ispined,:wght,:wfsecid,:wfsecname,:RedFreq,:RedNotice)";

            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["dev"].ConnectionString);
            //string _constr = "User Id=AHAIKIN;Password=ahaikin;Data Source=NYCORADEV01.etp.local/data001;Validate Connection=true";
            //OracleConnection connection = new OracleConnection(_constr);
            OracleCommand command = new OracleCommand(commandString, connection);
            command.ArrayBindCount = dt.Rows.Count;

            //apprvlstat
            OracleParameter approvalStatusParam = new OracleParameter("apprvlstat", OracleDbType.Varchar2);
            approvalStatusParam.Direction = ParameterDirection.Input;
            approvalStatusParam.Value = dt.AsEnumerable().Select(r => r.Field<string>(0)).ToArray();
            command.Parameters.Add(approvalStatusParam);

            //baseamnt
            OracleParameter baseAmountParam = new OracleParameter("baseamnt", OracleDbType.Decimal);
            baseAmountParam.Direction = ParameterDirection.Input;
            baseAmountParam.Value = dt.AsEnumerable().Select(r => r.Field<double>("BASE_AMOUNT")).ToArray();
            command.Parameters.Add(baseAmountParam);

            //evldt
            OracleParameter evalDateParam = new OracleParameter("evldt", OracleDbType.Date);
            evalDateParam.Direction = ParameterDirection.Input;
            evalDateParam.Value = dt.AsEnumerable().Select(r => r.Field<DateTime>("EVAL_DATE")).ToArray();
            command.Parameters.Add(evalDateParam);

            //evlid
            OracleParameter evalIdParam = new OracleParameter("evlid", OracleDbType.Int32);
            evalIdParam.Direction = ParameterDirection.Input;
            evalIdParam.Value = dt.AsEnumerable().Select(r => r.Field<int>("EVAL_ID")).ToArray();
            command.Parameters.Add(evalIdParam);

            //evlrnk
            OracleParameter evaluationRankParam = new OracleParameter("evlrnk", OracleDbType.Int32);
            evaluationRankParam.Direction = ParameterDirection.Input;
            evaluationRankParam.Value = dt.AsEnumerable().Select(r => r.Field<int>("EVALUATION_RANK")).ToArray();
            command.Parameters.Add(evaluationRankParam);

            //prdid
            OracleParameter insightProductIdParam = new OracleParameter("prdid", OracleDbType.Int32);
            insightProductIdParam.Direction = ParameterDirection.Input;
            insightProductIdParam.Value = dt.AsEnumerable().Select(r => r.Field<int>("INSIGHT_PRODUCT_ID")).ToArray();
            command.Parameters.Add(insightProductIdParam);

            //prdnm
            OracleParameter insightProductNameParam = new OracleParameter("prdnm", OracleDbType.Varchar2);
            insightProductNameParam.Direction = ParameterDirection.Input;
            insightProductNameParam.Value = dt.AsEnumerable().Select(r => r.Field<string>("INSIGHT_PRODUCT_NAME")).ToArray();
            command.Parameters.Add(insightProductNameParam);

            //isofficial
            OracleParameter isOfficialParam = new OracleParameter("isofficial", OracleDbType.Int32);
            isOfficialParam.Direction = ParameterDirection.Input;
            isOfficialParam.Value = dt.AsEnumerable().Select(r => r.Field<int>("IS_OFFICIAL")).ToArray();
            command.Parameters.Add(isOfficialParam);

            //ispined
            OracleParameter isPinedParam = new OracleParameter("ispined", OracleDbType.Int32);
            isPinedParam.Direction = ParameterDirection.Input;
            isPinedParam.Value = dt.AsEnumerable().Select(r => r.Field<int>("IS_PINED")).ToArray();
            command.Parameters.Add(isPinedParam);


            //wght
            OracleParameter weightParam = new OracleParameter("wght", OracleDbType.Decimal);
            weightParam.Direction = ParameterDirection.Input;
            weightParam.Value = dt.AsEnumerable().Select(r => r.Field<double>("WEIGHT")).ToArray();
            command.Parameters.Add(weightParam);

            //wfsecid
            OracleParameter wfSecurityIdParam = new OracleParameter("wfsecid", OracleDbType.Int32);
            wfSecurityIdParam.Direction = ParameterDirection.Input;
            wfSecurityIdParam.Value = dt.AsEnumerable().Select(r => r.Field<int>("WF_SECURITY_ID")).ToArray();
            command.Parameters.Add(wfSecurityIdParam);

            //wfsecname
            OracleParameter wfSecurityNameParam = new OracleParameter("wfsecname", OracleDbType.Varchar2);
            wfSecurityNameParam.Direction = ParameterDirection.Input;
            wfSecurityNameParam.Value = dt.AsEnumerable().Select(r => r.Field<string>("WF_SECURITY_NAME")).ToArray();
            command.Parameters.Add(wfSecurityNameParam);

            //Red_Freq
            OracleParameter RedFreqParam = new OracleParameter("RedFreq", OracleDbType.Varchar2);
            RedFreqParam.Direction = ParameterDirection.Input;
            RedFreqParam.Value = dt.AsEnumerable().Select(r => r.Field<string>("REDEMPTION_FREQUENCY")).ToArray();
            command.Parameters.Add(RedFreqParam);


            //Red_Freq
            OracleParameter RedNotice= new OracleParameter("RedNotice", OracleDbType.Varchar2);
            RedNotice.Direction = ParameterDirection.Input;
            RedNotice.Value = dt.AsEnumerable().Select(r => r.Field<int>("REDEMPTION_NOTICE")).ToArray();
            command.Parameters.Add(RedNotice);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine("aslfhklaf");
            }
            finally
            {
                connection.Close();
                command.Dispose();
                connection.Dispose();

            }
        }
    }
}
namespace Extensions.DateTime
{
    public static class BusinessDays
    {
        public static System.DateTime AddBusinessDays(this System.DateTime source, int businessDays)
        {
            var dayOfWeek = businessDays < 0
                                ? ((int)source.DayOfWeek - 12) % 7
                                : ((int)source.DayOfWeek + 6) % 7;

            switch (dayOfWeek)
            {
                case 6:
                    businessDays--;
                    break;
                case -6:
                    businessDays++;
                    break;
            }

            return source.AddDays(businessDays + ((businessDays + dayOfWeek) / 5) * 2);
        }
    }
}